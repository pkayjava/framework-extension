package com.angkorteam.framework.jdbc;

import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by socheatkhauv on 29/1/17.
 */
public class SelectQuery extends WhereQuery<SelectQuery> {

    protected final String tableName;

    protected final List<String> field = new LinkedList<>();
    protected final List<String> join = new LinkedList<>();
    protected final List<String> orderBy = new LinkedList<>();
    protected final List<String> having = new LinkedList<>();
    protected final List<String> groupBy = new LinkedList<>();
    protected boolean pagination = false;
    protected long offset;
    protected long number;
    protected final boolean forUpdate;

    public SelectQuery(String tableName) {
        this(tableName, false);
    }

    public SelectQuery(String tableName, boolean forUpdate) {
        this.tableName = tableName;
        this.forUpdate = forUpdate;
    }

    public SelectQuery setOffset(Long offset) {
        this.offset = offset;
        this.dirty = true;
        this.pagination = true;
        return this;
    }

    public Long getOffset() {
        return this.offset;
    }

    public SelectQuery setNumber(long number) {
        this.number = number;
        this.dirty = true;
        this.pagination = true;
        return this;
    }

    public Long getNumber() {
        return this.number;
    }

    public SelectQuery addField(String field) {
        this.field.add(field);
        this.dirty = true;
        return this;
    }

    public SelectQuery addField(String... fields) {
        if (fields != null && fields.length > 0) {
            for (String field : fields) {
                addField(field);
            }
            this.dirty = true;
        }
        return this;
    }

    public SelectQuery addHaving(String criteria) {
        if (criteria.contains(":")) {
            throw new RuntimeException(": is invalid");
        }
        this.having.add(criteria);
        this.dirty = true;
        return this;
    }

    public SelectQuery addHaving(String criteria, Object value) {
        addInternalCriteria(this.having, this.param, criteria, value);
        return this;
    }

    public <T> SelectQuery addHaving(String criteria, Class<T> typeClass, List<T> values) {
        addInternalCriteria(this.having, this.param, criteria, typeClass, values);
        return this;
    }

    public <T> SelectQuery addHaving(String criteria, Class<T> typeClass, T value1, T value2) {
        addInternalCriteria(this.having, this.param, criteria, typeClass, value1, value2);
        return this;
    }

    public SelectQuery addOrderBy(String orderBy) {
        this.orderBy.add(orderBy);
        this.dirty = true;
        return this;
    }

    public SelectQuery addOrderBy(String field, SortType sort) {
        if (sort == SortType.Asc) {
            this.orderBy.add(field + " ASC");
        } else if (sort == SortType.Desc) {
            this.orderBy.add(field + " DESC");
        }
        this.dirty = true;
        return this;
    }

    public SelectQuery addGroupBy(String field) {
        this.groupBy.add(field);
        this.dirty = true;
        return this;
    }

    public SelectQuery addJoin(String join) {
        this.join.add(join);
        return this;
    }

    public SelectQuery addJoin(JoinType join, String tableName, String on) {
        if (join == JoinType.LeftJoin) {
            this.join.add("LEFT JOIN " + tableName + " ON " + on);
        } else if (join == JoinType.RightJoin) {
            this.join.add("RIGHT JOIN " + tableName + " ON " + on);
        } else if (join == JoinType.InnerJoin) {
            this.join.add("INNER JOIN " + tableName + " ON " + on);
        }
        this.dirty = true;
        return this;
    }

    public SelectQuery setLimit(Long offset, Long number) {
        this.offset = offset;
        this.number = number;
        this.dirty = true;
        this.pagination = true;
        return this;
    }

    public boolean isForUpdate() {
        return forUpdate;
    }

    public String toSQL() {
        if (!this.dirty) {
            return this.cached;
        }
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ");
        if (this.field.isEmpty()) {
            this.field.add("*");
        }
        builder.append(StringUtils.join(this.field, ", "));
        builder.append(" FROM ");
        builder.append(this.tableName);
        if (!this.join.isEmpty()) {
            builder.append(" ").append(StringUtils.join(this.join, " "));
        }
        if (!this.where.isEmpty()) {
            builder.append(" WHERE ").append(StringUtils.join(this.where, " AND "));
        }
        if (!this.groupBy.isEmpty()) {
            builder.append(" GROUP BY ").append(StringUtils.join(this.groupBy, ", "));
        }
        if (!this.having.isEmpty()) {
            builder.append(" HAVING ").append(StringUtils.join(this.having, " AND "));
        }
        if (!this.orderBy.isEmpty()) {
            builder.append(" ORDER BY ").append(StringUtils.join(this.orderBy, ", "));
        }
        if (this.pagination) {
            builder.append(" LIMIT ").append(this.offset).append(",").append(this.number);
        }
        if (this.forUpdate) {
            builder.append(" FOR UPDATE");
        }
        this.cached = builder.toString();
        this.dirty = false;
        return this.cached;
    }

}
