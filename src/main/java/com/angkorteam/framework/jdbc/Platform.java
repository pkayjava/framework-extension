package com.angkorteam.framework.jdbc;

/**
 * Created by socheatkhauv on 3/12/17.
 */
public enum Platform {

    MySQL, Oracle, SQLServer, MariaDB, PostgreSQL

}
