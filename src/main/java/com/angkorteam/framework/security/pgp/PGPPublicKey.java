package com.angkorteam.framework.security.pgp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.CompressionAlgorithmTags;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPLiteralDataGenerator;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPContentVerifierBuilderProvider;
import org.bouncycastle.openpgp.operator.jcajce.JcePGPDataEncryptorBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyKeyEncryptionMethodGenerator;

import com.angkorteam.framework.security.PublicKey;

public class PGPPublicKey implements PublicKey {

    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    protected org.bouncycastle.openpgp.PGPPublicKey publicKey;

    protected PGPPublicKey(org.bouncycastle.openpgp.PGPPublicKey publicKey) {
        this.publicKey = publicKey;
    }

    @Override
    public String getEncoded() {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ArmoredOutputStream stream = new ArmoredOutputStream(out);
            this.publicKey.encode(stream);
            stream.close();
            return new String(out.toByteArray());
        } catch (IOException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public boolean verify(String message, String signature) {
        PGPSignature s = null;
        try {
            try (InputStream decoderStream = PGPUtil.getDecoderStream(new ByteArrayInputStream(signature.getBytes()))) {
                JcaPGPObjectFactory objectFactory = new JcaPGPObjectFactory(decoderStream);
                Object object = objectFactory.nextObject();
                PGPSignatureList signatures;
                if (object instanceof PGPCompressedData) {
                    PGPCompressedData c1 = (PGPCompressedData) object;
                    objectFactory = new JcaPGPObjectFactory(c1.getDataStream());
                    signatures = (PGPSignatureList) objectFactory.nextObject();
                } else {
                    signatures = (PGPSignatureList) object;
                }
                s = signatures.get(0);
            }
            JcaPGPContentVerifierBuilderProvider builder = new JcaPGPContentVerifierBuilderProvider();
            builder.setProvider(BouncyCastleProvider.PROVIDER_NAME);
            boolean verified;
            s.init(builder, publicKey);
            s.update(message.getBytes());
            verified = s.verify();
            return verified;
        } catch (IOException | PGPException e) {
            throw new UnsupportedOperationException(e);
        }

    }

    @Override
    public String encrypt(String message) {
        byte[] data = message.getBytes();
        ByteArrayInputStream clearData = new ByteArrayInputStream(data);
        ByteArrayOutputStream encryptData = new ByteArrayOutputStream();
        try {
            SecureRandom secureRandom = new SecureRandom();
            JcePGPDataEncryptorBuilder builder = new JcePGPDataEncryptorBuilder(PGPEncryptedData.CAST5);
            builder.setWithIntegrityPacket(true);
            builder.setSecureRandom(secureRandom);
            builder.setProvider(BouncyCastleProvider.PROVIDER_NAME);
            PGPEncryptedDataGenerator generator = new PGPEncryptedDataGenerator(builder);
            JcePublicKeyKeyEncryptionMethodGenerator methodGenerator = new JcePublicKeyKeyEncryptionMethodGenerator(this.publicKey);
            methodGenerator.setProvider(BouncyCastleProvider.PROVIDER_NAME);
            generator.addMethod(methodGenerator);

            ByteArrayOutputStream temp = new ByteArrayOutputStream();
            PGPCompressedDataGenerator comData = new PGPCompressedDataGenerator(CompressionAlgorithmTags.UNCOMPRESSED);
            OutputStream temp2 = comData.open(temp);
            PGPLiteralDataGenerator lData = new PGPLiteralDataGenerator();
            Date now = new Date();
            OutputStream pOut = lData.open(temp2, PGPLiteralData.BINARY, DateFormatUtils.ISO_8601_EXTENDED_TIME_TIME_ZONE_FORMAT.format(now), data.length, now);
            IOUtils.copy(clearData, pOut);
            comData.close();
            byte[] temp5 = temp.toByteArray();

            try (OutputStream temp3 = new ArmoredOutputStream(encryptData)) {
                try (OutputStream temp4 = generator.open(temp3, temp5.length)) {
                    temp4.write(temp5);
                }
            }
            return new String(encryptData.toByteArray());
        } catch (IOException | PGPException e) {
            throw new UnsupportedOperationException(e);
        }
    }

}
