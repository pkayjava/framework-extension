package com.angkorteam.framework.security.x509;

import java.security.InvalidKeyException;
import java.security.Signature;
import java.security.SignatureException;

import org.springframework.util.Base64Utils;

import com.angkorteam.framework.security.PrivateKey;

public class X509PrivateKey implements PrivateKey {

    protected java.security.PrivateKey privateKey;

    protected X509PrivateKey(java.security.PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    @Override
    public String getEncoded() {
        return Base64Utils.encodeToString(this.privateKey.getEncoded());
    }

    @Override
    public String sign(String message) {
        try {
            Signature signature = X509Signature.getInstance();
            signature.initSign(this.privateKey);
            signature.update(message.getBytes());
            return Base64Utils.encodeToString(signature.sign());
        } catch (SignatureException | InvalidKeyException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public String decrypt(String message) {
        throw new UnsupportedOperationException("decrypt is not supported");
    }

}
