package com.angkorteam.framework.security.x509;

import java.security.InvalidKeyException;
import java.security.Signature;
import java.security.SignatureException;

import org.springframework.util.Base64Utils;

import com.angkorteam.framework.security.PublicKey;

public class X509PublicKey implements PublicKey {

    protected java.security.PublicKey publicKey;

    protected X509PublicKey(java.security.PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    @Override
    public String getEncoded() {
        return Base64Utils.encodeToString(this.publicKey.getEncoded());
    }

    @Override
    public boolean verify(String message, String signature) {
        try {
            Signature s = X509Signature.getInstance();
            s.initVerify(this.publicKey);
            s.update(message.getBytes());
            return s.verify(Base64Utils.decodeFromString(signature));
        } catch (SignatureException | InvalidKeyException e) {
            return false;
        }
    }

    @Override
    public String encrypt(String message) {
        throw new UnsupportedOperationException("encrypt is not supported");
    }

}
