package com.angkorteam.framework.security.x509;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class X509Signature {

    private static final String ALGORITHM = "SHA256withRSA";

    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    public static java.security.Signature getInstance() {
        try {
            return java.security.Signature.getInstance(ALGORITHM, BouncyCastleProvider.PROVIDER_NAME);
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new UnsupportedOperationException(e);
        }
    }
}
