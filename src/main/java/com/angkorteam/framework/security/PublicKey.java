package com.angkorteam.framework.security;

public interface PublicKey extends Key {

    public boolean verify(String message, String signature);

    public String encrypt(String message);

}
