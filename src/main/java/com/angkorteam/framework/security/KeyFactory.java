package com.angkorteam.framework.security;

import java.io.File;

public interface KeyFactory {

    public PrivateKey generatePrivate(String privateKey);

    public PrivateKey generatePrivate(File privateKey);

    public PublicKey generatePublic(String publicKey);

    public PublicKey generatePublic(File publicKey);

}
