package com.angkorteam.framework.security;

public interface PrivateKey extends Key {

    public String sign(String message);

    public String decrypt(String message);

}
