package com.angkorteam.framework.security;

public interface Key {

    public String getEncoded();

}
