package com.angkorteam.framework;

/**
 * Created by socheatkhauv on 6/12/17.
 */
public enum NavBarMenuItemType {

    ItemPanel, ItemImage, ItemIcon, ItemText, ItemProgressBar;

}
