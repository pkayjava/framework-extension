package com.angkorteam.framework.wicket.markup.html.form;

import com.angkorteam.framework.ReferenceUtilities;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.model.IModel;

import java.util.Date;

public class DayMonthTextField extends org.apache.wicket.extensions.markup.html.form.DateTextField {

    private static final String JAVA_PARTTERN = "dd MMMM";
    private static final String JAVASCRIPT_PARTTERN = "dd MM";

    public DayMonthTextField(String id, IModel<Date> model) {
        super(id, model, JAVA_PARTTERN);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        setOutputMarkupId(true);
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        String markupId = this.getMarkupId(true);
        response.render(CssHeaderItem.forReference(DateTextField.CSS));
        response.render(CssHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.DATE_PICKER_CSS)));
        response.render(JavaScriptHeaderItem.forReference(getApplication().getJavaScriptLibrarySettings().getJQueryReference()));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.BOOTSTRAP_JS)));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.DATE_PICKER_JS)));
        response.render(OnDomReadyHeaderItem.forScript("$('#" + markupId + "').datepicker({ todayBtn:'linked', todayHighlight:true, autoclose:true, format:'" + JAVASCRIPT_PARTTERN + "', weekStart:1 })"));
    }
}