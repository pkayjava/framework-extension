package com.angkorteam.framework.wicket.markup.html.form;
/**
 * Created by Khauv Socheat on 4/14/2016.
 */

import com.angkorteam.framework.ReferenceUtilities;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.model.IModel;

public class HtmlTextArea extends TextArea<String> {
    public HtmlTextArea(String id) {
        super(id);
    }

    public HtmlTextArea(String id, IModel<String> model) {
        super(id, model);
    }

    protected void onInitialize() {
        super.onInitialize();
        this.setOutputMarkupId(true);
    }

    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        String markupId = this.getMarkupId(true);
        response.render(CssHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "lib/codemirror.css")));
        response.render(CssHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "addon/hint/show-hint.css")));
        response.render(CssHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "addon/display/fullscreen.css")));
        response.render(CssHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "theme/night.css")));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "lib/codemirror.js")));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "addon/hint/show-hint.js")));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "addon/hint/css-hint.js")));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "addon/hint/html-hint.js")));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "mode/xml/xml.js")));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "mode/javascript/javascript.js")));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "mode/css/css.js")));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "mode/vbscript/vbscript.js")));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "mode/htmlmixed/htmlmixed.js")));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "addon/display/fullscreen.js")));
        response.render(OnDomReadyHeaderItem.forScript("CodeMirror.fromTextArea(document.getElementById('" + markupId + "'), { indentUnit: 4, lineNumbers: true, theme: 'night', extraKeys: {'Ctrl-Space': 'autocomplete', 'F11': function(cm) { cm.setOption('fullScreen', !cm.getOption('fullScreen')); }, 'Esc': function(cm) { if (cm.getOption('fullScreen')) cm.setOption('fullScreen', false); }}, mode: {name: 'htmlmixed', globalVars: true} })"));
    }
}