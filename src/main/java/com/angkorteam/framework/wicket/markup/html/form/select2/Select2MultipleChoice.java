package com.angkorteam.framework.wicket.markup.html.form.select2;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.IRequestListener;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.MarkupStream;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.IRequestParameters;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.http.WebResponse;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.ResourceReference;
import org.apache.wicket.util.convert.ConversionException;
import org.apache.wicket.util.convert.IConverter;
import org.apache.wicket.util.string.AppendingStringBuffer;
import org.apache.wicket.util.string.Strings;

import com.angkorteam.framework.ReferenceUtilities;
import com.angkorteam.framework.SpringBean;
import com.google.gson.Gson;

/**
 * Created by socheat on 7/25/15.
 */
public class Select2MultipleChoice<T> extends FormComponent<Collection<T>> implements IRequestListener {

    public static final ResourceReference CSS = new CssResourceReference(
            Option.class, "select2.css");

    private boolean validValue;

    private boolean invalidValue;

    private boolean convertValue;

    private final MultipleChoiceProvider<T> provider;

    private transient Collection<T> convertedInput;

    private int inputLength;

    public static final String REMOVE_POPUP_UP_SCRIPT = Select2SingleChoice.REMOVE_POPUP_UP_SCRIPT;

    public Select2MultipleChoice(String id, IModel<Collection<T>> object, MultipleChoiceProvider<T> provider) {
        this(id, 0, object, provider);
    }

    public Select2MultipleChoice(String id, int inputLength, IModel<Collection<T>> object, MultipleChoiceProvider<T> provider) {
        super(id, object);
        this.provider = provider;
        this.inputLength = inputLength;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(AttributeModifier.replace("multiple", "multiple"));
        setOutputMarkupId(true);
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        response.render(CssHeaderItem.forReference(CSS));
        response.render(CssHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.SELECT2_CSS)));
        response.render(JavaScriptHeaderItem.forReference(getApplication().getJavaScriptLibrarySettings().getJQueryReference()));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.SELECT2_JS)));
        String markupId = getMarkupId();
        String url = this.urlForListener(new PageParameters()).toString();
        if (isRequired()) {
            StringResourceModel selectOne = new StringResourceModel("null", this);
            response.render(OnDomReadyHeaderItem.forScript("$('#" + markupId + "')." + "select2({placeholder:'" + selectOne.getString() + "', ajax: { url: '" + url + "', dataType: 'json', delay: 250, data: function (params) { return { q: params.term, page: params.page }; }, processResults: function (data, params) { params.page = params.page || 1; return { results: data.items, pagination: { more: data.more } }; }, cache: true }, escapeMarkup: function (markup) { return markup; }, minimumInputLength: " + this.inputLength + "});"));
        } else {
            response.render(OnDomReadyHeaderItem.forScript("$('#" + markupId + "')." + "select2({placeholder:'', ajax: { url: '" + url + "', dataType: 'json', delay: 250, data: function (params) { return { q: params.term, page: params.page }; }, processResults: function (data, params) { params.page = params.page || 1; return { results: data.items, pagination: { more: data.more } }; }, cache: true }, escapeMarkup: function (markup) { return markup; }, minimumInputLength: " + this.inputLength + "});"));
        }
    }

    @Override
    protected void onComponentTag(ComponentTag tag) {
        super.onComponentTag(tag);
        String clazz = tag.getAttribute("class");
        if (clazz == null || "".equals(clazz)) {
            clazz = "select2";
        } else {
            if (!clazz.contains("select2")) {
                clazz = clazz + " " + "select2";
            }
        }
        if (clazz != null && !"".equals(clazz)) {
            tag.put("class", clazz);
        }

        String style = tag.getAttribute("style");
        if (style == null || "".equals(style)) {
            style = "width: 100%;";
        } else {
            if (!style.contains("width: 100%")) {
                if (style.endsWith(";")) {
                    style = style + "width: 100%;";
                } else {
                    style = style + ";width: 100%;";
                }
            }
        }
        if (style != null && !"".equals(style)) {
            tag.put("style", style);
        }
    }

    @Override
    protected Collection<T> convertValue(String[] value) throws ConversionException {
        this.convertValue = true;
        if (value != null && value.length > 0) {
            this.convertedInput = this.provider.toChoices(Arrays.asList(value));
            return this.convertedInput;
        } else {
            if (this.convertedInput != null) {
                this.convertedInput.clear();
            }
            return null;
        }

    }

    @Override
    public void onComponentTagBody(MarkupStream markupStream, ComponentTag openTag) {
        if (!this.invalidValue && !this.validValue && !this.convertValue) {
            Collection<T> choices = getModelObject();
            final AppendingStringBuffer buffer = new AppendingStringBuffer("");
            if (choices != null && !choices.isEmpty()) {
                int i = 0;
                for (T choice : choices) {
                    i++;
                    appendOptionHtml(buffer, choice, i);
                }
            }
            buffer.append('\n');
            replaceComponentTagBody(markupStream, openTag, buffer);
        } else {
            if (this.invalidValue) {
                if (this.convertValue) {
                    final AppendingStringBuffer buffer = new AppendingStringBuffer("");
                    if (this.convertedInput != null && !this.convertedInput.isEmpty()) {
                        int i = 0;
                        for (T choice : this.convertedInput) {
                            i++;
                            appendOptionHtml(buffer, choice, i);
                        }
                    }
                    buffer.append('\n');
                    replaceComponentTagBody(markupStream, openTag, buffer);
                }
            }
            if (this.validValue) {
                Collection<T> choices = getModelObject();
                final AppendingStringBuffer buffer = new AppendingStringBuffer("");
                if (choices != null && !choices.isEmpty()) {
                    int i = 0;
                    for (T choice : choices) {
                        i++;
                        appendOptionHtml(buffer, choice, i);
                    }
                }
                buffer.append('\n');
                replaceComponentTagBody(markupStream, openTag, buffer);
            }
        }
    }

    protected void appendOptionHtml(AppendingStringBuffer buffer, T choice, int index) {
        Object objectValue = this.provider.getDisplayValue(choice);
        Class<?> objectClass = (objectValue == null ? null : objectValue.getClass());

        String displayValue = "";
        if (objectClass != null && objectClass != String.class) {
            @SuppressWarnings("rawtypes")
            IConverter converter = getConverter(objectClass);
            displayValue = converter.convertToString(objectValue, getLocale());
        } else if (objectValue != null) {
            displayValue = objectValue.toString();
        }

        buffer.append("\n<option ");
        setOptionAttributes(buffer, choice, index);
        buffer.append('>');

        String display = displayValue;
        if (localizeDisplayValues()) {
            display = getLocalizer().getString(displayValue, this, displayValue);
        }

        CharSequence escaped = display;
        if (getEscapeModelStrings()) {
            escaped = escapeOptionHtml(display);
        }

        buffer.append(escaped);
        buffer.append("</option>");
    }

    protected CharSequence escapeOptionHtml(String displayValue) {
        return Strings.escapeMarkup(displayValue);
    }

    protected boolean localizeDisplayValues() {
        return false;
    }

    protected void setOptionAttributes(AppendingStringBuffer buffer, T choice, int index) {
        buffer.append("selected=\"selected\" ");
        buffer.append("value=\"");
        buffer.append(Strings.escapeMarkup(provider.getIdValue(choice, index)));
        buffer.append('"');
    }

    @Override
    public void onRequest() {
        RequestCycle requestCycle = RequestCycle.get();
        Request request = requestCycle.getRequest();
        IRequestParameters params = request.getRequestParameters();

        Gson gson = getGson();

        String term = params.getParameterValue("q").toOptionalString();
        term = StringUtils.trimToEmpty(term);
        int page = params.getParameterValue("page").toInt(1);

        List<Option> options = this.provider.doQuery(term, page);

        Select2Response response = new Select2Response();
        response.setMore(this.provider.hasMore(term, page));
        response.setItems(options);

        WebResponse webResponse = (WebResponse) requestCycle.getResponse();
        webResponse.setContentType("application/json");

        OutputStreamWriter stream = new OutputStreamWriter(webResponse.getOutputStream(), getRequest().getCharset());
        try {
            stream.write(gson.toJson(response));
            stream.flush();
        } catch (IOException e) {
        }
    }

    @Override
    protected void onValid() {
        super.onValid();
        this.validValue = true;
    }

    @Override
    protected void onInvalid() {
        super.onInvalid();
        this.invalidValue = true;
    }

    @Override
    protected void onAfterRender() {
        super.onAfterRender();
        this.validValue = false;
        this.invalidValue = false;
        this.convertValue = false;
    }

    public int getInputLength() {
        return inputLength;
    }

    protected Gson getGson() {
        return SpringBean.getBean(Gson.class);
    }

    @Override
    public boolean rendersPage() {
        return false;
    }
}
