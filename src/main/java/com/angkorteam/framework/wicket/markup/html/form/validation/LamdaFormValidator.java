package com.angkorteam.framework.wicket.markup.html.form.validation;

import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.markup.html.form.FormComponent;

import com.angkorteam.framework.wicket.functional.WicketConsumer;
import com.angkorteam.framework.wicket.markup.html.form.Form;

public class LamdaFormValidator implements org.apache.wicket.markup.html.form.validation.IFormValidator {

    protected FormComponent<?>[] components;

    protected WicketConsumer<Form<?>> validator;

    public LamdaFormValidator(WicketConsumer<Form<?>> validator, FormComponent<?> component) {
        if (component == null) {
            throw new WicketRuntimeException("component is required");
        }
        this.components = new FormComponent<?>[] { component };
        this.validator = validator;
    }

    public LamdaFormValidator(WicketConsumer<Form<?>> validator, FormComponent<?>... components) {
        if (components == null || components.length == 0) {
            throw new WicketRuntimeException("components is required");
        }
        this.components = components;
        this.validator = validator;
    }

    @Override
    public FormComponent<?>[] getDependentFormComponents() {
        return this.components;
    }

    @Override
    public void validate(org.apache.wicket.markup.html.form.Form<?> form) {
        if (this.validator != null) {
            this.validator.accept((Form<?>) form);
        }
    }

}
