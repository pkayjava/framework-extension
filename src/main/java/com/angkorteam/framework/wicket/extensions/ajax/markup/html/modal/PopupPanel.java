package com.angkorteam.framework.wicket.extensions.ajax.markup.html.modal;

import java.util.Map;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.panel.Panel;

public abstract class PopupPanel extends Panel {

	protected final String name;

	protected final Map<String, Object> model;

	protected ModalWindow window;

	public PopupPanel(String name, Map<String, Object> model) {
		super(ModalWindow.CONTENT_ID);
		this.name = name;
		this.model = model;
	}

	protected final void closeWindow(String signalId, AjaxRequestTarget target) {
		if (this.window != null) {
			this.window.setSignalId(signalId);
			if (target != null) {
				this.window.close(target);
			}
		}
	}

	@Override
	protected final void onInitialize() {
		initData();
		super.onInitialize();
		initComponent();
		configureMetaData();
	}

	protected abstract void initData();

	protected abstract void initComponent();

	protected abstract void configureMetaData();

}
