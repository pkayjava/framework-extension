package com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class CellPanel extends Panel {

    public CellPanel(String id) {
        super(id);
    }

    public CellPanel(String id, IModel<?> model) {
        super(id, model);
    }

}
