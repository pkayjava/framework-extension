package com.angkorteam.framework.wicket.extensions.markup.html.tabs;

public abstract class ITab implements org.apache.wicket.extensions.markup.html.tabs.ITab {

    protected AjaxTabbedPanel<? extends ITab> tab;

    public AjaxTabbedPanel<? extends ITab> getTab() {
        return this.tab;
    }

    public abstract boolean isEnabled();

}
