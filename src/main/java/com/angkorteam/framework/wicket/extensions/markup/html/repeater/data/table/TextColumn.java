package com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table;

import com.angkorteam.framework.share.provider.TableProvider;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.Calendar;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.CellPanel;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemClass;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemPanel;
import com.angkorteam.framework.wicket.functional.WicketTriFunction;

import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import java.util.Map;

/**
 * Created by socheat on 12/7/16.
 */
public class TextColumn extends AbstractColumn<Map<String, Object>, String> {

    private WicketTriFunction<String, IModel<String>, Map<String, Object>, ItemPanel> function;

    private String valueExpression;

    public TextColumn(TableProvider provider, ItemClass itemClass, IModel<String> displayModel, String valueColumn, String propertyExpression, WicketTriFunction<String, IModel<String>, Map<String, Object>, ItemPanel> function) {
        super(displayModel, propertyExpression);
        this.function = function;
        this.valueExpression = valueColumn;
        if (itemClass == ItemClass.Byte) {
            provider.selectField(valueColumn, Byte.class);
        } else if (itemClass == ItemClass.Short) {
            provider.selectField(valueColumn, Short.class);
        } else if (itemClass == ItemClass.Integer) {
            provider.selectField(valueColumn, Integer.class);
        } else if (itemClass == ItemClass.Long) {
            provider.selectField(valueColumn, Long.class);
        } else if (itemClass == ItemClass.Float) {
            provider.selectField(valueColumn, Float.class);
        } else if (itemClass == ItemClass.Double) {
            provider.selectField(valueColumn, Double.class);
        } else if (itemClass == ItemClass.Boolean) {
            provider.selectField(valueColumn, Boolean.class);
        } else if (itemClass == ItemClass.String) {
            provider.selectField(valueColumn, String.class);
        } else if (itemClass == ItemClass.DateTime) {
            provider.selectField(valueColumn, "yyyy-MM-dd HH:mm:ss", Calendar.DateTime);
        } else if (itemClass == ItemClass.Date) {
            provider.selectField(valueColumn, "yyyy-MM-dd", Calendar.Date);
        } else if (itemClass == ItemClass.Time) {
            provider.selectField(valueColumn, "HH:mm:ss", Calendar.Time);
        }
    }

    public TextColumn(IModel<String> displayModel, String valueExpression, String propertyExpression, WicketTriFunction<String, IModel<String>, Map<String, Object>, ItemPanel> function) {
        super(displayModel, propertyExpression);
        this.function = function;
        this.valueExpression = valueExpression;
    }

    @Override
    public void populateItem(Item<ICellPopulator<Map<String, Object>>> item, String componentId, IModel<Map<String, Object>> rowModel) {
        ItemPanel itemPanel = this.function.apply(this.valueExpression, this.getDisplayModel(), rowModel.getObject());
        if (itemPanel == null) {
            item.add(new Label(componentId, ""));
        } else {
            CellPanel cell = new CellPanel(componentId);
            item.add(cell);
            cell.add(itemPanel);
        }
    }

}
