package com.angkorteam.framework.wicket.ajax.form;

import org.apache.wicket.ajax.AjaxRequestTarget;

import com.angkorteam.framework.wicket.functional.WicketBiFunction;
import com.angkorteam.framework.wicket.functional.WicketFunction;
import com.angkorteam.framework.wicket.markup.html.form.DateTextField;
import com.angkorteam.framework.wicket.markup.html.form.select2.Select2SingleChoice;

/**
 * Created by socheatkhauv on 7/15/17.
 */
public class OnChangeAjaxBehavior extends org.apache.wicket.ajax.form.OnChangeAjaxBehavior {

    private WicketFunction<AjaxRequestTarget, Boolean> update;

    private WicketBiFunction<AjaxRequestTarget, RuntimeException, Boolean> error;

    public OnChangeAjaxBehavior() {
    }

    public OnChangeAjaxBehavior(WicketFunction<AjaxRequestTarget, Boolean> update) {
        this.update = update;
    }

    public OnChangeAjaxBehavior(WicketFunction<AjaxRequestTarget, Boolean> update, WicketBiFunction<AjaxRequestTarget, RuntimeException, Boolean> error) {
        this.update = update;
        this.error = error;
    }

    @Override
    protected final void onUpdate(AjaxRequestTarget target) {
        if (this.update != null) {
            boolean clear = this.update.apply(target);
            if (clear) {
                target.appendJavaScript(Select2SingleChoice.REMOVE_POPUP_UP_SCRIPT);
                target.appendJavaScript(DateTextField.REMOVE_POPUP_UP_SCRIPT);
            }
        }
    }

    @Override
    protected final void onError(AjaxRequestTarget target, RuntimeException e) {
        if (this.error != null) {
            boolean clear = this.error.apply(target, e);
            if (clear) {
                target.appendJavaScript(Select2SingleChoice.REMOVE_POPUP_UP_SCRIPT);
                target.appendJavaScript(DateTextField.REMOVE_POPUP_UP_SCRIPT);
            }
        } else {
            super.onError(target, e);
        }
    }

}
