package com.angkorteam.framework.wicket.ajax.markup.html;

import com.angkorteam.framework.wicket.functional.WicketBiFunction;
import com.angkorteam.framework.wicket.markup.html.form.DateTextField;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.model.IModel;

import com.angkorteam.framework.wicket.markup.html.form.select2.Select2SingleChoice;

/**
 * Created by socheat on 10/23/15.
 */
public class AjaxLink<T> extends org.apache.wicket.ajax.markup.html.AjaxLink<T> {

    private WicketBiFunction<AjaxLink, AjaxRequestTarget, Boolean> onClick;

    public AjaxLink(String id) {
        super(id);
    }

    public AjaxLink(String id, IModel<T> model) {
        super(id, model);
    }

    @Override
    public final void onClick(AjaxRequestTarget target) {
        if (this.onClick != null) {
            boolean clear = this.onClick.apply(this, target);
            if (clear) {
                target.appendJavaScript(Select2SingleChoice.REMOVE_POPUP_UP_SCRIPT);
                target.appendJavaScript(DateTextField.REMOVE_POPUP_UP_SCRIPT);
            }
        }
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        setOutputMarkupId(true);
    }

    public void setOnClick(WicketBiFunction<AjaxLink, AjaxRequestTarget, Boolean> onClick) {
        this.onClick = onClick;
    }

}
