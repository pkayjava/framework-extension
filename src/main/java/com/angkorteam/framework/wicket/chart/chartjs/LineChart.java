package com.angkorteam.framework.wicket.chart.chartjs;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebComponent;
import org.apache.wicket.markup.parser.XmlTag.TagType;
import org.apache.wicket.model.IModel;

import com.angkorteam.framework.ReferenceUtilities;
import com.google.gson.Gson;

public class LineChart extends WebComponent {

	private IModel<LineDataset> dataset;

	public LineChart(String id, IModel<LineDataset> model) {
		super(id);
		setOutputMarkupId(true);
		this.dataset = model;
	}

	@Override
	protected void onComponentTag(final ComponentTag tag) {
		checkComponentTag(tag, "canvas");
		super.onComponentTag(tag);
		if (tag.isOpenClose()) {
			tag.setType(TagType.OPEN);
		}
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);

		ReferenceUtilities.renderJavascript(response, "<!-- ChartJS 1.0.2 -->");
		ReferenceUtilities.renderJavascript(response, "AdminLTE/bower_components/chart.js/Chart.min.js");

		Gson gson = new Gson();

		String markupId = getMarkupId();
		String chart = String.format("new Chart(document.getElementById('%s').getContext('2d')).Line(%s, { responsive : true })", markupId, gson.toJson(this.dataset.getObject()));
		response.render(OnDomReadyHeaderItem.forScript(chart));
	}

	public static class LineItem implements Serializable {

		private String pointHighlightStroke;

		private String pointHighlightFill;

		private String pointStrokeColor;

		private String pointColor;

		private String strokeColor;

		private String fillColor;

		private List<Integer> data = new LinkedList<>();

		public List<Integer> getData() {
			return data;
		}

		public String getPointHighlightStroke() {
			return pointHighlightStroke;
		}

		public void setPointHighlightStroke(String pointHighlightStroke) {
			this.pointHighlightStroke = pointHighlightStroke;
		}

		public String getPointHighlightFill() {
			return pointHighlightFill;
		}

		public void setPointHighlightFill(String pointHighlightFill) {
			this.pointHighlightFill = pointHighlightFill;
		}

		public String getPointStrokeColor() {
			return pointStrokeColor;
		}

		public void setPointStrokeColor(String pointStrokeColor) {
			this.pointStrokeColor = pointStrokeColor;
		}

		public String getPointColor() {
			return pointColor;
		}

		public void setPointColor(String pointColor) {
			this.pointColor = pointColor;
		}

		public String getStrokeColor() {
			return strokeColor;
		}

		public void setStrokeColor(String strokeColor) {
			this.strokeColor = strokeColor;
		}

		public String getFillColor() {
			return fillColor;
		}

		public void setFillColor(String fillColor) {
			this.fillColor = fillColor;
		}

	}

	public static class LineDataset implements Serializable {

		private List<String> labels = new LinkedList<>();

		private List<LineItem> datasets = new LinkedList<>();

		public List<String> getLabels() {
			return labels;
		}

		public List<LineItem> getDatasets() {
			return datasets;
		}

	}

}