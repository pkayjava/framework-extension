package com.angkorteam.framework.wicket.chart.chartjs;

import java.io.Serializable;
import java.util.List;

import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebComponent;
import org.apache.wicket.markup.parser.XmlTag.TagType;
import org.apache.wicket.model.IModel;

import com.angkorteam.framework.ReferenceUtilities;
import com.google.gson.Gson;

public class PieChart extends WebComponent {

	private IModel<List<PieItem>> dataset;

	public PieChart(String id, IModel<List<PieItem>> model) {
		super(id);
		setOutputMarkupId(true);
		this.dataset = model;
	}

	@Override
	protected void onComponentTag(final ComponentTag tag) {
		checkComponentTag(tag, "canvas");
		super.onComponentTag(tag);
		if (tag.isOpenClose()) {
			tag.setType(TagType.OPEN);
		}
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);

		ReferenceUtilities.renderJavascript(response, "<!-- ChartJS 1.0.2 -->");
		ReferenceUtilities.renderJavascript(response, "AdminLTE/bower_components/chart.js/Chart.min.js");

		Gson gson = new Gson();

		String markupId = getMarkupId();

		String chart = String.format("new Chart(document.getElementById('%s').getContext('2d')).Pie(%s)", markupId, gson.toJson(this.dataset.getObject()));
		response.render(OnDomReadyHeaderItem.forScript(chart));
	}

	public static class PieItem implements Serializable {

		private double value;

		private String color;

		private String highlight;

		private String label;

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public double getValue() {
			return value;
		}

		public void setValue(double value) {
			this.value = value;
		}

		public String getColor() {
			return color;
		}

		public void setColor(String color) {
			this.color = color;
		}

		public String getHighlight() {
			return highlight;
		}

		public void setHighlight(String highlight) {
			this.highlight = highlight;
		}

	}

}