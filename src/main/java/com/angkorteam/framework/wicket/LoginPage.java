package com.angkorteam.framework.wicket;

import java.io.Serializable;

import org.apache.wicket.MetaDataKey;
import org.apache.wicket.Page;
import org.apache.wicket.Session;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.protocol.http.WebApplication;

import com.angkorteam.framework.Function;
import com.angkorteam.framework.AdminLTEOption;
import com.angkorteam.framework.ReferenceUtilities;
import com.angkorteam.framework.models.Login;
import com.angkorteam.framework.wicket.markup.html.form.Button;
import com.angkorteam.framework.wicket.markup.html.form.Form;
import com.angkorteam.framework.wicket.markup.html.link.Link;
import com.angkorteam.framework.wicket.widget.ReadOnlyView;
import com.angkorteam.framework.wicket.widget.TextFeedbackPanel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class LoginPage extends WebPage {

	public static final LoginMetaDataKey LOGIN = new LoginMetaDataKey();

	public static final ResetMetaDataKey RESET = new ResetMetaDataKey();

	public static final RegisterMetaDataKey REGISTER = new RegisterMetaDataKey();

	private TextField<String> loginField;
	private String loginValue;

	private TextField<String> passwordField;
	private String passwordValue;

	private CheckBox rememberField;
	private boolean rememberValue;

	protected String versionValue;
	protected ReadOnlyView versionView;

	private Form<Void> form;
	private Button loginButton;

	@Override
	protected void onInitialize() {
		super.onInitialize();
		this.form = new Form<>("form");
		this.add(this.form);

		this.versionValue = "VERSION : " + Function.getVersion(WebApplication.get().getServletContext());

		this.loginField = new TextField<>("loginField", new PropertyModel<>(this, "loginValue"));
		this.loginField.setLabel(Model.of("login"));
		this.loginField.setRequired(true);
		this.form.add(this.loginField);
		TextFeedbackPanel loginFeedback = new TextFeedbackPanel("loginFeedback", this.loginField);
		this.form.add(loginFeedback);

		this.passwordField = new PasswordTextField("passwordField", new PropertyModel<>(this, "passwordValue"));
		this.passwordField.setRequired(true);
		this.passwordField.setLabel(Model.of("password"));
		this.form.add(this.passwordField);
		TextFeedbackPanel passwordFeedback = new TextFeedbackPanel("passwordFeedback", this.passwordField);
		this.form.add(passwordFeedback);

		this.rememberField = new CheckBox("rememberField", new PropertyModel<>(this, "rememberValue"));
		this.form.add(rememberField);

		this.loginButton = new Button("loginButton");
		this.form.add(this.loginButton);
		this.loginButton.setOnSubmit(this::loginButtonClick);

		IReset reset = getApplication().getMetaData(RESET);

		IRegister register = getApplication().getMetaData(REGISTER);

		WebMarkupContainer extraLink = new WebMarkupContainer("extraLink");
		this.add(extraLink);

		if (reset == null && register == null) {
			extraLink.setVisible(false);
			BookmarkablePageLink<Void> resetLink = new BookmarkablePageLink<>("resetLink", LoginPage.class);
			BookmarkablePageLink<Void> registerLink = new BookmarkablePageLink<>("registerLink", LoginPage.class);
			extraLink.add(resetLink);
			extraLink.add(registerLink);
		} else {
			if (reset == null) {
				BookmarkablePageLink<Void> resetLink = new BookmarkablePageLink<>("resetLink", LoginPage.class);
				resetLink.setVisible(false);
				extraLink.add(resetLink);
			} else {
				Link<Void> resetLink = new Link<>("resetLink");
				extraLink.add(resetLink);
				resetLink.setOnClick(this::resetLinkClick);
			}
			if (reset == null) {
				BookmarkablePageLink<Void> registerLink = new BookmarkablePageLink<>("registerLink", LoginPage.class);
				registerLink.setVisible(false);
				extraLink.add(registerLink);
			} else {
				Link<Void> registerLink = new Link<>("registerLink");
				registerLink.setOnClick(this::registerLinkClick);
				extraLink.add(registerLink);
			}
		}

		this.versionView = new ReadOnlyView("versionView", new PropertyModel<String>(this, "versionValue"));
		add(this.versionView);

	}

	protected void resetLinkClick(Link<Void> link) {

	}

	protected void registerLinkClick(Link<Void> link) {

	}

	protected void loginButtonClick(Button button) {
		ILogin login = getApplication().getMetaData(LOGIN);
		if (login != null) {
			Login value = new Login(this.loginValue, this.passwordValue, this.rememberValue);
			login.login(getSession(), this, value, this.loginField, this.passwordField);
		} else {
			throw new WicketRuntimeException("Application.setMetaData(LoginPage.LOGIN, ILogin)");
		}
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);
		AdminLTEOption option = new AdminLTEOption();
		option.Bootstrap = true;
		option.FontAwesome = true;
		option.Ionicons = true;
		option.AdminLTE = true;
		option.ICheck = true;
		option.HTML5ShimAndRespondJS = true;
		option.GoogleFont = true;
		option.JQuery = true;
		ReferenceUtilities.render(option, response);
	}

	public static interface ILogin extends Serializable {

		public void login(Session session, Page page, Login login, TextField<String> loginField,
				TextField<String> passwordField);

	}

	public static interface IReset extends Serializable {

		public void reset();

	}

	public static interface IRegister extends Serializable {

		public void register();

	}

	public static class LoginMetaDataKey extends MetaDataKey<ILogin> {
	}

	public static class ResetMetaDataKey extends MetaDataKey<IReset> {
	}

	public static class RegisterMetaDataKey extends MetaDataKey<IRegister> {
	}

}
