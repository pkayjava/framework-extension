package com.angkorteam.framework.wicket.functional;

import org.apache.wicket.util.io.IClusterable;

import java.util.function.Supplier;

public interface WicketSupplier<R> extends Supplier<R>, IClusterable {
}
