package com.angkorteam.framework.wicket.functional;

import java.util.function.Function;

import org.apache.wicket.util.io.IClusterable;

public interface WicketFunction<T, R> extends Function<T, R>, IClusterable {
}
