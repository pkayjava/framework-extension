package com.angkorteam.framework.spring;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;
import java.io.File;

/**
 * Created by socheatkhauv on 29/1/17.
 */
public class ResourceFactoryBean implements FactoryBean<Resource>, InitializingBean, ServletContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceFactoryBean.class);

    private Resource resource;

    private ServletContext servletContext;

    private String filename;

    @Override
    public Resource getObject() throws Exception {
        return this.resource;
    }

    @Override
    public Class<?> getObjectType() {
        return Resource.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (this.servletContext == null) {
            throw new IllegalArgumentException("servlet context is null");
        }
        String configurationFile = this.servletContext.getInitParameter("configuration");
        File file;
        if (!Strings.isNullOrEmpty(configurationFile)) {
            file = new File(configurationFile);
            LOGGER.info("resource is using servlet init param \"configuration\"");
        } else {
            File home = new File(System.getProperty("user.home"));
            if (Strings.isNullOrEmpty(this.filename)) {
                throw new Exception("filename is required when servlet init param \"configuration\" is not found");
            }
            file = new File(this.filename);
            if (!file.exists()) {
                file = new File(home, ".xml/" + this.filename);
            }
            LOGGER.info("resource is using configuration from user home directory");
        }
        if (!file.exists()) {
            throw new Exception(file.getAbsolutePath() + " is not found");
        }
        this.resource = new FileSystemResource(file);
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
