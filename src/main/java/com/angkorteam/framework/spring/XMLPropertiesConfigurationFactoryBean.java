package com.angkorteam.framework.spring;

import com.google.common.base.Strings;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLPropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;
import java.io.File;

/**
 * Created by socheatkhauv on 29/1/17.
 */
public class XMLPropertiesConfigurationFactoryBean implements FactoryBean<XMLPropertiesConfiguration>, InitializingBean, ServletContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(XMLPropertiesConfigurationFactoryBean.class);

    private XMLPropertiesConfiguration configuration;

    private ServletContext servletContext;

    private String filename;

    @Override
    public XMLPropertiesConfiguration getObject() throws Exception {
        return this.configuration;
    }

    @Override
    public Class<?> getObjectType() {
        return XMLPropertiesConfiguration.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (this.servletContext == null) {
            throw new IllegalArgumentException("servlet context is null");
        }
        String configurationFile = this.servletContext.getInitParameter("configuration");
        File file;
        if (!Strings.isNullOrEmpty(configurationFile)) {
            file = new File(configurationFile);
            LOGGER.info("configuration is using servlet init param \"configuration\"");
        } else {
            File home = new File(System.getProperty("user.home"));
            if (Strings.isNullOrEmpty(this.filename)) {
                throw new Exception("filename is required when servlet init param \"configuration\" is not found");
            }
            file = new File(this.filename);
            if (!file.exists()) {
                file = new File(home, ".xml/" + this.filename);
            }
            LOGGER.info("configuration is using servlet init param \"configuration\"");
        }
        if (!file.exists()) {
            throw new Exception(file.getAbsolutePath() + " is not found");
        }
        try {
            this.configuration = new XMLPropertiesConfiguration(file);
        } catch (ConfigurationException e) {
            throw new Exception("could not load " + file.getAbsolutePath(), e);
        }
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

}
