package com.angkorteam.framework.spring;

import java.sql.Connection;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import com.google.common.base.Strings;

/**
 * Created by socheatkhauv on 4/2/17.
 */
public class DelegateLocalFactoryBean implements FactoryBean<Delegate>, InitializingBean, DisposableBean {

	private String driverClassName;

	private String username;

	private String password;

	private int maxIdle = 10;

	private int minIdle = 5;

	private int maxWaitMillis = 5000;

	private int maxTotal = 20;

	private int initialSize = 5;

	private String url;

	private boolean autoCommit = false;

	private boolean autoCommitOnReturn = true;

	private int transactionIsolation = Connection.TRANSACTION_READ_COMMITTED;

	private Delegate delegate;

	@Override
	public Delegate getObject() throws Exception {
		return this.delegate;
	}

	@Override
	public Class<?> getObjectType() {
		return Delegate.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (Strings.isNullOrEmpty(this.driverClassName)) {
			throw new Exception("driverClassName is required");
		}
		if (Strings.isNullOrEmpty(this.url)) {
			throw new Exception("url is required");
		}
		if (Strings.isNullOrEmpty(this.username)) {
			throw new Exception("username is required");
		}
		this.delegate = new Delegate(getDataSource(this.driverClassName, this.url, username, password));

	}

	protected BasicDataSource getDataSource(String jdbcDriver, String jdbcUrl, String username, String password) {
		BasicDataSource delegate = new BasicDataSource();
		delegate.setDriverClassName(jdbcDriver);
		delegate.setUsername(username);
		delegate.setPassword(password);
		delegate.setUrl(jdbcUrl);
		delegate.setMaxIdle(this.maxIdle);
		delegate.setMinIdle(this.minIdle);
		delegate.setMaxWaitMillis(this.maxWaitMillis);
		delegate.setMaxTotal(this.maxTotal);
		delegate.setInitialSize(this.initialSize);
		delegate.setDefaultAutoCommit(this.autoCommit);
		delegate.setEnableAutoCommitOnReturn(this.autoCommitOnReturn);
		delegate.setDefaultTransactionIsolation(this.transactionIsolation);
		return delegate;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setMaxIdle(int maxIdle) {
		this.maxIdle = maxIdle;
	}

	public void setMinIdle(int minIdle) {
		this.minIdle = minIdle;
	}

	public void setMaxWaitMillis(int maxWaitMillis) {
		this.maxWaitMillis = maxWaitMillis;
	}

	public void setMaxTotal(int maxTotal) {
		this.maxTotal = maxTotal;
	}

	public void setInitialSize(int initialSize) {
		this.initialSize = initialSize;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setAutoCommit(boolean autoCommit) {
		this.autoCommit = autoCommit;
	}

	public void setAutoCommitOnReturn(boolean autoCommitOnReturn) {
		this.autoCommitOnReturn = autoCommitOnReturn;
	}

	public void setTransactionIsolation(int transactionIsolation) {
		this.transactionIsolation = transactionIsolation;
	}

	@Override
	public void destroy() throws Exception {
		if (this.delegate != null) {
			this.delegate.close();
		}
	}

}
