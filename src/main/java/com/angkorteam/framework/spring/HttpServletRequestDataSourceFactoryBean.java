package com.angkorteam.framework.spring;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import com.angkorteam.framework.HttpServletRequestDataSource;

public class HttpServletRequestDataSourceFactoryBean implements FactoryBean<DataSource>, InitializingBean, DisposableBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpServletRequestDataSourceFactoryBean.class);

    private DataSource delegate;

    private HttpServletRequestDataSource dataSource;

    @Override
    public DataSource getObject() throws Exception {
        return this.dataSource;
    }

    @Override
    public Class<?> getObjectType() {
        return DataSource.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    public void setDelegate(DataSource delegate) {
        this.delegate = delegate;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (this.delegate == null) {
            throw new Exception("delegate is required");
        }
        this.dataSource = new HttpServletRequestDataSource(this.delegate);
    }

    @Override
    public void destroy() throws Exception {
        if (this.dataSource != null && this.dataSource instanceof HttpServletRequestDataSource) {
            try (Connection connection = this.dataSource.getConnection()) {
                if (connection != null) {
                    try {
                        connection.commit();
                    } catch (SQLException e) {
                        LOGGER.info("could not commit transaction due to this reason {}", e.getMessage());
                        connection.rollback();
                    }
                }
            }
        }
    }
}
