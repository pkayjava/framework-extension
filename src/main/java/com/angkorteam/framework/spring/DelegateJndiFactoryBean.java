package com.angkorteam.framework.spring;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import com.google.common.base.Strings;

/**
 * Created by socheatkhauv on 4/2/17.
 */
public class DelegateJndiFactoryBean implements FactoryBean<Delegate>, InitializingBean {

	private String jndi;

	private Delegate delegate;

	@Override
	public Delegate getObject() throws Exception {
		return this.delegate;
	}

	@Override
	public Class<?> getObjectType() {
		return Delegate.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void afterPropertiesSet() throws Exception {

		if (Strings.isNullOrEmpty(jndi)) {
			throw new Exception("jndi is required");
		}
		Context initContext = new InitialContext();
		Context envContext = null;
		try {
			envContext = (Context) initContext.lookup("java:/comp/env");
		} catch (NamingException e) {
			throw new Exception("java:/comp/env is not found", e);
		}
		try {
			DataSource dataSource = (DataSource) envContext.lookup(this.jndi);
			this.delegate = new Delegate(dataSource);
		} catch (NamingException e) {
			throw new Exception(this.jndi + " is not found", e);
		}

	}

	public void setJndi(String jndi) {
		this.jndi = jndi;
	}

}
