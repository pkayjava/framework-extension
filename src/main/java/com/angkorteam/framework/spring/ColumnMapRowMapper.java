package com.angkorteam.framework.spring;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Map;

import org.springframework.jdbc.support.JdbcUtils;

public class ColumnMapRowMapper extends org.springframework.jdbc.core.ColumnMapRowMapper {

    @Override
    public Map<String, Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnCount = rsmd.getColumnCount();
        Map<String, Object> mapOfColValues = createColumnMap(columnCount);
        for (int i = 1; i <= columnCount; i++) {
            String key = getColumnKey(JdbcUtils.lookupColumnName(rsmd, i));
            String tableName = rsmd.getTableName(i);
            Object obj = getColumnValue(rs, i);
            if (obj != null) {
                if (obj instanceof Byte || obj instanceof Short || obj instanceof Integer) {
                    mapOfColValues.put(key, ((Number) obj).longValue());
                    if (tableName != null && !"".equals(tableName)) {
                        mapOfColValues.put(tableName + "." + key, ((Number) obj).longValue());
                    }
                } else if (obj instanceof Float) {
                    mapOfColValues.put(key, ((Number) obj).doubleValue());
                    if (tableName != null && !"".equals(tableName)) {
                        mapOfColValues.put(tableName + "." + key, ((Number) obj).doubleValue());
                    }
                } else if (obj instanceof BigInteger) {
                    mapOfColValues.put(key, ((Number) obj).longValue());
                    if (tableName != null && !"".equals(tableName)) {
                        mapOfColValues.put(tableName + "." + key, ((Number) obj).longValue());
                    }
                } else if (obj instanceof BigDecimal) {
                    mapOfColValues.put(key, ((Number) obj).doubleValue());
                    if (tableName != null && !"".equals(tableName)) {
                        mapOfColValues.put(tableName + "." + key, ((Number) obj).doubleValue());
                    }
                } else {
                    mapOfColValues.put(key, obj);
                    if (tableName != null && !"".equals(tableName)) {
                        mapOfColValues.put(tableName + "." + key, obj);
                    }
                }
            } else {
                mapOfColValues.put(key, obj);
                if (tableName != null && !"".equals(tableName)) {
                    mapOfColValues.put(tableName + "." + key, obj);
                }
            }
        }
        return mapOfColValues;
    }

    @Override
    protected Map<String, Object> createColumnMap(int columnCount) {
        return new LinkedCaseInsensitiveMap<>(columnCount);
    }

}
