package com.angkorteam.framework;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.wicket.WicketRuntimeException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Created by socheat on 3/27/16.
 */
public class Function {

	public static String getHttpAddress(HttpServletRequest request) {
		ServletContext servletContext = request.getServletContext();
		StringBuffer address = new StringBuffer();
		if (request.isSecure() && request.getServerPort() == 443) {
			address.append("https://").append(request.getServerName()).append(servletContext.getContextPath());
		} else if (!request.isSecure() && request.getServerPort() == 80) {
			address.append("http://").append(request.getServerName()).append(servletContext.getContextPath());
		} else {
			if (request.isSecure()) {
				address.append("https://");
			} else {
				address.append("http://");
			}
			address.append(request.getServerName()).append(":").append(request.getServerPort())
					.append(servletContext.getContextPath());
		}
		String result = address.toString();
		if (result.endsWith("/")) {
			return result.substring(0, result.length() - 1);
		} else {
			return result;
		}
	}

	public static String getVersion(ServletContext servletContext) {
		String manifest = "/META-INF/MANIFEST.MF";
		String absoluteDiskPath = servletContext.getRealPath(manifest);
		File file = new File(absoluteDiskPath);

		if (file.exists() && file.isFile()) {
			try {
				List<String> lines = FileUtils.readLines(file, "UTF-8");
				String v = "";
				String t = "";
				for (String line : lines) {
					if (line != null && !"".equals(line)) {
						if (line.startsWith("Version")) {
							v = StringUtils.trimToEmpty(StringUtils.substring(line, "Version:".length()));
						} else if (line.startsWith("Build-Time")) {
							t = StringUtils.trimToEmpty(StringUtils.substring(line, "Build-Time:".length()));
						}
					}
				}
				return v + " " + t;
			} catch (IOException e) {
				throw new WicketRuntimeException(e);
			}
		} else {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;

			try {
				builder = factory.newDocumentBuilder();
			} catch (ParserConfigurationException e) {
				throw new WicketRuntimeException(e);
			}

			File popFile = new File("pom.xml");

			Document document = null;
			try {
				document = builder.parse(popFile);
			} catch (SAXException | IOException e) {
				throw new WicketRuntimeException(e);
			}

			return document.getDocumentElement().getElementsByTagName("version").item(0).getTextContent() + " "
					+ DateFormatUtils.ISO_8601_EXTENDED_DATETIME_TIME_ZONE_FORMAT.format(new Date());
		}
	}
}
