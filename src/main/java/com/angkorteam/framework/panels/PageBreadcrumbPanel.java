package com.angkorteam.framework.panels;

import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;

import com.angkorteam.framework.Emoji;
import com.angkorteam.framework.models.PageBreadcrumb;

/**
 * Created by socheatkhauv on 6/11/17.
 */
public class PageBreadcrumbPanel extends Panel {

	private WebMarkupContainer wicketContainer;

	private RepeatingView item;

	public PageBreadcrumbPanel(String id, IModel<List<PageBreadcrumb>> model) {
		super(id, model);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		this.wicketContainer = new WebMarkupContainer("wicketContainer");
		this.add(this.wicketContainer);

		this.item = new RepeatingView("item");
		this.wicketContainer.add(this.item);

		List<PageBreadcrumb> pageBreadcrumbs = (List<PageBreadcrumb>) getDefaultModelObject();
		if (pageBreadcrumbs == null || pageBreadcrumbs.isEmpty()) {
			this.wicketContainer.setVisible(false);
		} else {
			for (PageBreadcrumb item : pageBreadcrumbs) {
				if (item.getPage() != null) {
					if (item.getIcon() != null) {
						String chileId = this.item.newChildId();
						Fragment fragment = new Fragment(chileId, "fragmentLinkIconLabel", this);
						this.item.add(fragment);
						BookmarkablePageLink<Void> link = new BookmarkablePageLink<>("link", item.getPage(),
								item.getParameters());
						fragment.add(link);
						WebMarkupContainer icon = new WebMarkupContainer("icon");
						if (item.getIcon().getType() == Emoji.FA) {
							icon.add(AttributeModifier.append("class", () -> "fa " + item.getIcon().getLiteral()));
						} else {
							icon.add(AttributeModifier.append("class", () -> "ion " + item.getIcon().getLiteral()));
						}
						link.add(icon);
						Label label = new Label("label", item.getLabel());
						link.add(label);
					} else {
						String chileId = this.item.newChildId();
						Fragment fragment = new Fragment(chileId, "fragmentLinkLabel", this);
						this.item.add(fragment);
						BookmarkablePageLink<Void> link = new BookmarkablePageLink<>("link", item.getPage(),
								item.getParameters());
						fragment.add(link);
						Label label = new Label("label", item.getLabel());
						link.add(label);
					}
				} else {
					if (item.getIcon() != null) {
						String chileId = this.item.newChildId();
						Fragment fragment = new Fragment(chileId, "fragmentIconLabel", this);
						this.item.add(fragment);
						WebMarkupContainer icon = new WebMarkupContainer("icon");
						if (item.getIcon().getType() == Emoji.FA) {
							icon.add(AttributeModifier.append("class", () -> "fa " + item.getIcon().getLiteral()));
						} else {
							icon.add(AttributeModifier.append("class", () -> "ion " + item.getIcon().getLiteral()));
						}
						fragment.add(icon);
						Label label = new Label("label", item.getLabel());
						fragment.add(label);
					} else {
						String chileId = this.item.newChildId();
						Fragment fragment = new Fragment(chileId, "fragmentLabel", this);
						this.item.add(fragment);
						Label label = new Label("label", item.getLabel());
						fragment.add(label);
					}
				}
			}
		}
	}
}
