package com.angkorteam.framework.panels;

import com.angkorteam.framework.Emoji;
import com.angkorteam.framework.ReferenceUtilities;
import com.angkorteam.framework.models.UserInfo;
import com.google.common.base.Strings;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.ExternalImage;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/13/17.
 */
public class UserInfoPanel extends Panel {

    private WebMarkupContainer wicketContainer;

    public UserInfoPanel(String id, IModel<UserInfo> model) {
        super(id, model);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        this.wicketContainer = new WebMarkupContainer("wicketContainer");
        this.add(this.wicketContainer);
        UserInfo userInfo = (UserInfo) getDefaultModelObject();
        if (userInfo == null) {
            this.wicketContainer.setVisible(false);
            Label title = new Label("title");
            this.wicketContainer.add(title);
            ExternalImage image = new ExternalImage("image");
            this.wicketContainer.add(image);
            Fragment fragment = new Fragment("item", "fragmentEmpty", this);
            this.wicketContainer.add(fragment);
        } else {
            if (Strings.isNullOrEmpty(userInfo.getImage())) {
                Image image = new Image("image", ReferenceUtilities.loadResourceReference("png/anonymous.png"));
                this.wicketContainer.add(image);
            } else {
                ExternalImage image = new ExternalImage("image", () -> userInfo.getImage());
                this.wicketContainer.add(image);
            }

            Label title = new Label("title", () -> userInfo.getTitle() == null ? "" : userInfo.getTitle());
            this.wicketContainer.add(title);
            if (Strings.isNullOrEmpty(userInfo.getTitle())) {
                title.setVisible(false);
            }

            Fragment fragment = null;
            if (userInfo.getPage() != null) {
                fragment = new Fragment("item", "fragmentLinkItem", this);
                BookmarkablePageLink<Void> link = new BookmarkablePageLink<>("link", userInfo.getPage(), userInfo.getParameters());
                fragment.add(link);

                Label icon = new Label("icon");
                link.add(icon);
                if (userInfo.getIcon() == null) {
                    icon.setVisible(false);
                } else {
                    if (userInfo.getIcon().getType() == Emoji.FA) {
                        icon.add(AttributeModifier.append("class", () -> "fa " + userInfo.getIcon().getLiteral()));
                    } else {
                        icon.add(AttributeModifier.append("class", () -> "ion " + userInfo.getIcon().getLiteral()));
                    }
                    if (userInfo.getIconColor() != null) {
                        icon.add(AttributeModifier.append("class", () -> userInfo.getIconColor().getLiteral()));
                    }
                }

                Label description = new Label("description", () -> userInfo.getDescription());
                link.add(description);
                if (Strings.isNullOrEmpty(userInfo.getDescription())) {
                    description.setVisible(false);
                }
            } else {
                fragment = new Fragment("item", "fragmentItem", this);
                Label icon = new Label("icon");
                fragment.add(icon);
                if (userInfo.getIcon() == null) {
                    icon.setVisible(false);
                } else {
                    if (userInfo.getIcon().getType() == Emoji.FA) {
                        icon.add(AttributeModifier.append("class", () -> "fa " + userInfo.getIcon().getLiteral()));
                    } else {
                        icon.add(AttributeModifier.append("class", () -> "ion " + userInfo.getIcon().getLiteral()));
                    }
                    if (userInfo.getIconColor() != null) {
                        icon.add(AttributeModifier.append("class", () -> userInfo.getIconColor().getLiteral()));
                    }
                }

                Label description = new Label("description", () -> userInfo.getDescription());
                fragment.add(description);
                if (Strings.isNullOrEmpty(userInfo.getDescription())) {
                    description.setVisible(false);
                }
            }
            this.wicketContainer.add(fragment);
        }

    }
}
