package com.angkorteam.framework.panels;

import com.angkorteam.framework.models.ProgressBar;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class ProgressBarPanel extends Panel {

    public ProgressBarPanel(String id) {
        super(id);
    }

    public ProgressBarPanel(String id, IModel<ProgressBar> model) {
        super(id, model);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        ProgressBar model = (ProgressBar) getDefaultModelObject();

        Fragment root = null;
        if (model.isVertical()) {
            root = new Fragment("root", "fragmentVertical", this);
            this.add(root);
        } else {
            root = new Fragment("root", "fragmentHorizontal", this);
            this.add(root);
        }

        WebMarkupContainer progressBar = new WebMarkupContainer("progressBar");
        root.add(progressBar);

        WebMarkupContainer progress = new WebMarkupContainer("progress");
        progressBar.add(progress);

        if (model.isActive()) {
            progressBar.add(AttributeModifier.append("class", "active"));
            progress.add(AttributeModifier.append("class", "progress-bar-striped"));
        }

        if (model.getColor() != null) {
            progress.add(AttributeModifier.append("class", model.getColor().getLiteral()));
        }

        if (model.getSize() != null) {
            progress.add(AttributeModifier.append("class", model.getSize().getLiteral()));
        }

        progress.add(AttributeModifier.replace("aria-valuemin", "0"));
        progress.add(AttributeModifier.replace("aria-valuemax", "100"));
        progress.add(AttributeModifier.replace("aria-valuenow", model.getValue()));

        if (model.isVertical()) {
            progress.add(AttributeModifier.replace("style", "height: " + model.getValue() + "%"));
        } else {
            progress.add(AttributeModifier.replace("style", "width: " + model.getValue() + "%"));
        }

        Label progressText = new Label("progressText", model.getValue() + "% Complete");
        progress.add(progressText);

    }
}
