package com.angkorteam.framework.panels;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class BoxFooterPanel extends Panel {

    private boolean clearFix;

    private boolean textCenter;

    public BoxFooterPanel(boolean clearFix, boolean textCenter) {
        super("footer");
        this.clearFix = clearFix;
        this.textCenter = textCenter;
    }

    public BoxFooterPanel(boolean clearFix, boolean textCenter, IModel<?> model) {
        super("footer", model);
        this.clearFix = clearFix;
        this.textCenter = textCenter;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        if (this.clearFix) {
            this.add(AttributeModifier.append("class", "clearfix"));
        }
        if (this.textCenter) {
            this.add(AttributeModifier.append("class", "text-center"));
        }
    }
}
