package com.angkorteam.framework.panels;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class InfoBoxExtraPanel extends Panel {

    public InfoBoxExtraPanel() {
        super("extra");
    }

    public InfoBoxExtraPanel(IModel<?> model) {
        super("extra", model);
    }

}
