package com.angkorteam.framework.panels;

import com.angkorteam.framework.models.Callout;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class CalloutPanel extends Panel {

    public CalloutPanel(String id) {
        super(id);
    }

    public CalloutPanel(String id, IModel<Callout> model) {
        super(id, model);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        WebMarkupContainer root = new WebMarkupContainer("root");
        add(root);

        Callout model = (Callout) getDefaultModelObject();

        WebMarkupContainer callout = new WebMarkupContainer("callout");
        root.add(callout);

        if (model.getType() != null) {
            callout.add(AttributeModifier.append("class", model.getType().getLiteral()));
        }

        Label title = new Label("title", model.getTitle());
        callout.add(title);

        Label description = new Label("description", model.getDescription());
        callout.add(description);

    }
}
