package com.angkorteam.framework.panels;

import java.util.Collections;
import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;

import com.angkorteam.framework.Emoji;
import com.angkorteam.framework.SideMenuType;
import com.angkorteam.framework.models.Badge;
import com.angkorteam.framework.models.SideMenu;

/**
 * Created by socheatkhauv on 6/14/17.
 */
public class SideMenuPanel extends Panel {

	private WebMarkupContainer wicketContainer;

	private RepeatingView root;

	public SideMenuPanel(String id, IModel<List<SideMenu>> model) {
		super(id, model);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		this.wicketContainer = new WebMarkupContainer("wicketContainer");
		this.add(this.wicketContainer);

		this.root = new RepeatingView("root");
		this.wicketContainer.add(this.root);

		List<SideMenu> sideMenus = (List<SideMenu>) getDefaultModelObject();

		SideMenu sideMenu = null;
		if (sideMenus != null) {
			for (SideMenu s : sideMenus) {
				if (s.getType() == SideMenuType.Header) {
					continue;
				}
				if (s.isActive() && s.getPage() != null) {
					sideMenu = s;
					break;
				} else {
					sideMenu = findActive(s);
					if (sideMenu != null && sideMenu.isActive() && sideMenu.getPage() != null) {
						break;
					}
				}
			}
		}
		if (sideMenu != null) {
			while (sideMenu.getParent() != null) {
				sideMenu.getParent().setActive(true);
				sideMenu = sideMenu.getParent();
			}
		}

		if (sideMenus == null || sideMenus.isEmpty()) {
			this.wicketContainer.setVisible(false);
		} else {
			for (SideMenu item : sideMenus) {
				if (item.getType() == SideMenuType.Header) {
					String childId = this.root.newChildId();
					Fragment fragment = new Fragment(childId, "fragmentHeader", this);
					this.root.add(fragment);
					processHeader(fragment, item);
				} else if (item.getType() == SideMenuType.Menu) {
					if (item.getPage() != null) {
						String childId = this.root.newChildId();
						Fragment fragment = new Fragment(childId, "fragmentItem", this);
						this.root.add(fragment);
						processItem(fragment, item);
					} else {
						String childId = this.root.newChildId();
						Fragment fragment = new Fragment(childId, "fragmentMenu", this);
						this.root.add(fragment);
						processMenu(fragment, item);
					}
				}
			}
		}
	}

	protected SideMenu findActive(SideMenu sideMenu) {
		if (sideMenu.getChildren() != null) {
			for (SideMenu ch : sideMenu.getChildren()) {
				if (ch.isActive()) {
					if (ch.getPage() != null) {
						return ch;
					} else {
						SideMenu t = findActive(ch);
						if (t != null && t.isActive() && t.getPage() != null) {
							return t;
						}
					}
				} else {
					if (ch.getPage() == null) {
						SideMenu t = findActive(ch);
						if (t != null && t.isActive() && t.getPage() != null) {
							return t;
						}
					}
				}
			}
		}
		return null;
	}

	protected void processMenu(MarkupContainer container, SideMenu sideMenu) {
		WebMarkupContainer root = new WebMarkupContainer("root");
		container.add(root);
		if (sideMenu.isActive()) {
			root.add(AttributeModifier.append("class", "active menu-open"));
		} else {
			root.add(AttributeModifier.append("class", "menu-close"));
		}

		Label icon = new Label("icon");
		root.add(icon);
		if (sideMenu.getIcon() == null) {
			icon.setVisible(false);
		} else {
			if (sideMenu.getIcon().getType() == Emoji.FA) {
				icon.add(AttributeModifier.append("class", "fa " + sideMenu.getIcon().getLiteral()));
			} else {
				icon.add(AttributeModifier.append("class", "ion " + sideMenu.getIcon().getLiteral()));
			}

			if (sideMenu.getIconColor() != null) {
				icon.add(AttributeModifier.append("class", () -> sideMenu.getIconColor().getLiteral()));
			}
		}
		Label label = new Label("label", () -> sideMenu.getLabel());
		root.add(label);

		RepeatingView child = new RepeatingView("child");
		root.add(child);
		for (SideMenu childItem : sideMenu.getChildren()) {
			if (childItem.getPage() != null) {
				String childId = child.newChildId();
				Fragment fragment = new Fragment(childId, "fragmentItem", this);
				child.add(fragment);
				processItem(fragment, childItem);
			} else {
				String childId = child.newChildId();
				Fragment fragment = new Fragment(childId, "fragmentMenu", this);
				child.add(fragment);
				processMenu(fragment, childItem);
			}
		}
	}

	protected void processMenu(SideMenu sideMenu) {

	}

	protected void processHeader(MarkupContainer container, SideMenu sideMenu) {
		Label label = new Label("label", () -> sideMenu.getLabel());
		container.add(label);
	}

	protected void processItem(MarkupContainer container, SideMenu sideMenu) {
		WebMarkupContainer root = new WebMarkupContainer("root");
		container.add(root);

		BookmarkablePageLink<Void> link = new BookmarkablePageLink<>("link", sideMenu.getPage(),
				sideMenu.getParameters());
		root.add(link);

		if (sideMenu.isActive()) {
			root.add(AttributeModifier.replace("class", "active"));
		}

		Label icon = new Label("icon");
		link.add(icon);
		if (sideMenu.getIcon() == null) {
			icon.setVisible(false);
		} else {
			if (sideMenu.getIcon().getType() == Emoji.FA) {
				icon.add(AttributeModifier.append("class", () -> "fa " + sideMenu.getIcon().getLiteral()));
			} else {
				icon.add(AttributeModifier.append("class", () -> "ion " + sideMenu.getIcon().getLiteral()));
			}
			if (sideMenu.getIconColor() != null) {
				icon.add(AttributeModifier.append("class", () -> sideMenu.getIconColor().getLiteral()));
			}
		}
		Label label = new Label("label", sideMenu.getLabel());
		link.add(label);

		WebMarkupContainer p = new WebMarkupContainer("p");
		link.add(p);
		if (sideMenu.getBadges() == null || sideMenu.getBadges().isEmpty()) {
			p.setVisible(false);
		}

		RepeatingView badges = new RepeatingView("badge");
		p.add(badges);
		if (sideMenu.getBadges() != null && !sideMenu.getBadges().isEmpty()) {
			Collections.sort(sideMenu.getBadges());
			for (int i = sideMenu.getBadges().size() - 1; i >= 0; i--) {
				Badge b = sideMenu.getBadges().get(i);
				String c = badges.newChildId();
				Fragment f = new Fragment(c, "fragmentBadge", this);
				badges.add(f);
				Label badge = new Label("badge", b.getBadgeText());
				f.add(badge);
				if (b.getBadgeColor() != null) {
					badge.add(AttributeModifier.append("class", b.getBadgeColor().getLiteral()));
				}
			}
		}
	}
}
