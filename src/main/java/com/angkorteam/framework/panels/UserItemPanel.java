package com.angkorteam.framework.panels;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/13/17.
 */
public class UserItemPanel extends Panel {

    public UserItemPanel() {
        super("item");
    }

    public UserItemPanel(IModel<?> model) {
        super("item", model);
    }
}
