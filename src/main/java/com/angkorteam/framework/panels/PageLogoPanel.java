package com.angkorteam.framework.panels;

import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

import com.angkorteam.framework.models.PageLogo;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class PageLogoPanel extends Panel {

	private WebMarkupContainer wicketContainer = null;

	public PageLogoPanel(String id, IModel<PageLogo> model) {
		super(id, model);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		this.wicketContainer = new WebMarkupContainer("wicketContainer");
		this.add(this.wicketContainer);

		PageLogo pageLogo = (PageLogo) getDefaultModelObject();
		if (pageLogo == null) {
			this.wicketContainer.setVisible(false);
		}
		MarkupContainer container = null;
		if (pageLogo != null && pageLogo.getPage() != null) {
			Fragment fragment = new Fragment("root", "fragmentLink", this);
			this.wicketContainer.add(fragment);
			container = new BookmarkablePageLink<Void>("link", pageLogo.getPage(), pageLogo.getParameters());
			fragment.add(container);
		} else {
			container = new Fragment("root", "fragmentText", this);
			this.wicketContainer.add(container);
		}
		parseItem(container, pageLogo);
	}

	protected void parseItem(MarkupContainer container, PageLogo pageLogo) {
		Label mini = new Label("mini",
				pageLogo == null || pageLogo.getSmall() == null || StringUtils.isEmpty(pageLogo.getSmall().getObject())
						? "<b>A</b>LT"
						: pageLogo.getSmall().getObject());
		mini.setEscapeModelStrings(false);
		container.add(mini);
		Label large = new Label("large",
				pageLogo == null || pageLogo.getLarge() == null || StringUtils.isEmpty(pageLogo.getLarge().getObject())
						? "<b>Admin</b>LTE"
						: pageLogo.getLarge().getObject());
		large.setEscapeModelStrings(false);
		container.add(large);
	}
}
