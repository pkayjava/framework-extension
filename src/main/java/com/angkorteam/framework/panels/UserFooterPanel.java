package com.angkorteam.framework.panels;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/13/17.
 */
public class UserFooterPanel extends Panel {

    public UserFooterPanel() {
        super("footer");
    }

    public UserFooterPanel(IModel<?> model) {
        super("footer", model);
    }
}
