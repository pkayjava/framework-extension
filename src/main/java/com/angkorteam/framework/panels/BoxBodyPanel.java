package com.angkorteam.framework.panels;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class BoxBodyPanel extends Panel {

    public BoxBodyPanel() {
        super("body");
    }

    public BoxBodyPanel(IModel<?> model) {
        super("body", model);
    }
}
