package com.angkorteam.framework.panels;

import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.ExternalImage;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;

import com.angkorteam.framework.Emoji;
import com.angkorteam.framework.EmojiColor;
import com.angkorteam.framework.NavBarMenuItemType;
import com.angkorteam.framework.NavBarMenuType;
import com.angkorteam.framework.models.NavBarMenu;
import com.angkorteam.framework.models.NavBarMenuItem;
import com.google.common.base.Strings;

/**
 * Created by socheatkhauv on 6/12/17.
 */
public class NavBarMenuPanel extends Panel {

	private WebMarkupContainer wicketContainer;

	private RepeatingView root;

	public NavBarMenuPanel(String id, IModel<List<NavBarMenu>> model) {
		super(id, model);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		this.wicketContainer = new WebMarkupContainer("wicketContainer");
		this.add(this.wicketContainer);

		this.root = new RepeatingView("root");
		this.wicketContainer.add(this.root);

		List<NavBarMenu> navBarMenus = (List<NavBarMenu>) getDefaultModelObject();
		if (navBarMenus == null || navBarMenus.isEmpty()) {
			this.wicketContainer.setVisible(false);
		} else {
			for (NavBarMenu item : navBarMenus) {
				if (item.getType() == NavBarMenuType.ItemIcon) {
					if (item.getChildren() == null || item.getChildren().isEmpty()) {
						String childId = this.root.newChildId();
						Fragment fragment = new Fragment(childId, "fragmentRootIcon", this);
						this.root.add(fragment);
						BookmarkablePageLink<Void> link = new BookmarkablePageLink<>("link", item.getPage(),
								item.getParameters());
						fragment.add(link);
						processItemIcon(link, item);
					} else {
						String childId = this.root.newChildId();
						Fragment fragment = new Fragment(childId, "fragmentRootChildrenIcon", this);
						this.root.add(fragment);
						processItemIcon(fragment, item);
						processHeaderItem(fragment, item);
						processBodyItem(fragment, item);
						processFooterItem(fragment, item);
					}
				} else if (item.getType() == NavBarMenuType.ItemImage) {
					if (item.getChildren() == null || item.getChildren().isEmpty()) {
						String childId = this.root.newChildId();
						Fragment fragment = new Fragment(childId, "fragmentRootImage", this);
						this.root.add(fragment);
						BookmarkablePageLink<Void> link = new BookmarkablePageLink<>("link", item.getPage(),
								item.getParameters());
						fragment.add(link);
						processItemImage(link, item);
					} else {
						String childId = this.root.newChildId();
						Fragment fragment = new Fragment(childId, "fragmentRootChildrenImage", this);
						this.root.add(fragment);
						processItemImage(fragment, item);
						processHeaderItem(fragment, item);
						processBodyItem(fragment, item);
						processFooterItem(fragment, item);
					}
				}
			}
		}
	}

	protected void processBodyItem(MarkupContainer parent, NavBarMenu root) {
		RepeatingView body = new RepeatingView("body");
		parent.add(body);
		for (NavBarMenuItem item : root.getChildren()) {
			if (item.getType() == NavBarMenuItemType.ItemIcon || item.getType() == NavBarMenuItemType.ItemText) {
				String childId = body.newChildId();
				Fragment fragment = new Fragment(childId, "fragmentItemIcon", this);
				body.add(fragment);
				BookmarkablePageLink<Void> link = new BookmarkablePageLink<>("link", item.getPage(),
						item.getParameters());
				fragment.add(link);
				processIcon(link, item.getIcon(), item.getIconColor());
				Label label = new Label("label", item.getLabel());
				link.add(label);
			} else if (item.getType() == NavBarMenuItemType.ItemImage) {
				String childId = body.newChildId();
				Fragment fragment = new Fragment(childId, "fragmentItemImage", this);
				body.add(fragment);
				BookmarkablePageLink<Void> link = new BookmarkablePageLink<>("link", item.getPage(),
						item.getParameters());
				fragment.add(link);
				ExternalImage image = new ExternalImage("image", item.getImage());
				link.add(image);
				if (item.getImage() == null || Strings.isNullOrEmpty(item.getImage().getObject())) {
					image.setVisible(false);
				}
				Label label = new Label("label", item.getLabel());
				link.add(label);
				Label description = new Label("description", item.getDescription());
				link.add(description);
				if (item.getDescription() == null || Strings.isNullOrEmpty(item.getDescription().getObject())) {
					description.setVisible(false);
				}
				Label smallIcon = new Label("smallIcon");
				link.add(smallIcon);
				if (item.getSmallIcon() == null) {
					smallIcon.setVisible(false);
				} else {
					if (item.getSmallIcon().getType() == Emoji.FA) {
						smallIcon.add(AttributeModifier.append("class", "fa " + item.getSmallIcon().getLiteral()));
					} else {
						smallIcon.add(AttributeModifier.append("class", "ion " + item.getSmallIcon().getLiteral()));
					}
					if (item.getSmallIconColor() != null) {
						smallIcon.add(AttributeModifier.append("class", item.getSmallIconColor().getLiteral()));
					}
				}
				Label small = new Label("small", item.getSmall());
				link.add(small);
			} else if (item.getType() == NavBarMenuItemType.ItemProgressBar) {
				String childId = body.newChildId();
				Fragment fragment = new Fragment(childId, "fragmentItemProgress", this);
				body.add(fragment);
				BookmarkablePageLink<Void> link = new BookmarkablePageLink<>("link", item.getPage(),
						item.getParameters());
				fragment.add(link);

				Label label = new Label("label", item.getLabel());
				link.add(label);

				Label small = new Label("small",
						() -> item.getProgressBarValue() == null ? "" : item.getProgressBarValue().getObject() + "%");
				link.add(small);

				WebMarkupContainer progressBar = new WebMarkupContainer("progressBar");
				link.add(progressBar);
				progressBar.add(AttributeModifier.replace("aria-valuenow", () -> item.getProgressBarValue()));
				progressBar.add(AttributeModifier.replace("style", () -> item.getProgressBarValue() == null ? ""
						: "width: " + item.getProgressBarValue().getObject() + "%"));
				if (item.getProgressBarColor() != null) {
					progressBar.add(AttributeModifier.append("class", item.getProgressBarColor().getLiteral()));
				}
				Label progressBarValue = new Label("progressBarValue", () -> item.getProgressBarValue() == null ? ""
						: item.getProgressBarValue().getObject() + "% Complete");
				progressBar.add(progressBarValue);
			} else if (item.getType() == NavBarMenuItemType.ItemPanel) {
				String childId = body.newChildId();
				Fragment fragment = new Fragment(childId, "fragmentItem", this);
				body.add(fragment);
				Panel panel = item.getPanel();
				fragment.add(panel);
			}
		}
	}

	protected void processHeaderItem(MarkupContainer parent, NavBarMenu item) {
		if (item.getHeaderPanel() == null) {
			Label header = new Label("header", item.getHeaderText());
			header.add(AttributeModifier.replace("class", "header"));
			parent.add(header);
			if (item.getHeaderText() == null || Strings.isNullOrEmpty(item.getHeaderText().getObject())) {
				header.setVisible(false);
			}
		} else {
			Panel header = item.getHeaderPanel();
			header.add(AttributeModifier.replace("class", "user-header"));
			parent.add(header);
		}

	}

	protected void processFooterItem(MarkupContainer parent, NavBarMenu item) {
		if (item.getFooterPanel() == null) {
			if (item.getFooterText() == null || Strings.isNullOrEmpty(item.getFooterText().getObject())) {
				Fragment fragment = new Fragment("footer", "fragmentEmpty", this);
				parent.add(fragment);
			} else {
				if (item.getFooterPage() == null) {
					Fragment fragment = new Fragment("footer", "fragmentFooterLabel", this);
					fragment.add(AttributeModifier.replace("class", "footer"));
					parent.add(fragment);
					Label label = new Label("label", item.getFooterText());
					fragment.add(label);
				} else {
					Fragment fragment = new Fragment("footer", "fragmentFooterLinkLabel", this);
					fragment.add(AttributeModifier.replace("class", "footer"));
					parent.add(fragment);
					BookmarkablePageLink<Void> link = new BookmarkablePageLink<>("link", item.getFooterPage(),
							item.getFooterParameters());
					fragment.add(link);
					Label label = new Label("label", item.getFooterText());
					link.add(label);
				}
			}
		} else {
			Panel footer = item.getFooterPanel();
			footer.add(AttributeModifier.replace("class", "user-footer"));
			parent.add(footer);
		}
	}

	protected void processItemIcon(MarkupContainer parent, NavBarMenu item) {
		processIcon(parent, item.getIcon(), item.getIconColor());
		processItem(parent, item);
	}

	protected void processIcon(MarkupContainer parent, Emoji emoji, EmojiColor color) {
		Label icon = new Label("icon");
		parent.add(icon);
		if (emoji == null) {
			icon.setVisible(false);
		} else {
			if (emoji.getType() == Emoji.FA) {
				icon.add(AttributeModifier.append("class", "fa " + emoji.getLiteral()));
			} else {
				icon.add(AttributeModifier.append("class", "ion " + emoji.getLiteral()));
			}
			if (color != null) {
				icon.add(AttributeModifier.append("class", color.getLiteral()));
			}
		}
	}

	protected void processItemImage(MarkupContainer parent, NavBarMenu item) {
		ExternalImage image = new ExternalImage("image", () -> item.getImage() == null ? "" : item.getImage());
		parent.add(image);
		if (item.getImage() == null || Strings.isNullOrEmpty(item.getImage().getObject())) {
			image.setVisible(false);
		}
		processItem(parent, item);
	}

	protected void processItem(MarkupContainer parent, NavBarMenu item) {
		Label label = new Label("label", item.getLabel());
		parent.add(label);
		if (item.getLabel() == null || Strings.isNullOrEmpty(item.getLabel().getObject())) {
			label.setVisible(false);
		}
		Label badge = new Label("badge", item.getBadge());
		parent.add(badge);
		if (item.getBadge() == null || Strings.isNullOrEmpty(item.getBadge().getObject())) {
			badge.setVisible(false);
		} else {
			if (item.getBadgeType() != null) {
				badge.add(AttributeModifier.append("class", () -> item.getBadgeType().getLiteral()));
			}
		}
	}
}
