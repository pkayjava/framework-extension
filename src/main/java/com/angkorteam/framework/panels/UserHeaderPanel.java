package com.angkorteam.framework.panels;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/13/17.
 */
public class UserHeaderPanel extends Panel {

    public UserHeaderPanel() {
        super("header");
    }

    public UserHeaderPanel(IModel<?> model) {
        super("header", model);
    }
}
