package com.angkorteam.framework;

/**
 * Created by socheatkhauv on 6/13/17.
 */
public enum AlertType {

    Warning("alert-warning"),

    Danger("alert-danger"),

    Info("alert-info"),

    Success("alert-success");

    private String literal;

    AlertType(String literal) {
        this.literal = literal;
    }

    public String getLiteral() {
        return literal;
    }

}
