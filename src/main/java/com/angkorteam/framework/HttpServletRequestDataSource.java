package com.angkorteam.framework;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.SmartDataSource;

/**
 * Created by socheatkhauv on 4/2/17.
 */
public class HttpServletRequestDataSource implements SmartDataSource {

    private final DataSource delegate;

    private final Connection connection;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(HttpServletRequestDataSource.class);

    public HttpServletRequestDataSource(DataSource delegate) {
        this.delegate = delegate;
        try {
            this.connection = delegate.getConnection();
            this.connection.setAutoCommit(false);
            this.connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        } catch (SQLException e) {
            LOGGER.info("issue {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        return this.connection;
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return this.connection;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return this.delegate.unwrap(iface);
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return this.delegate.isWrapperFor(iface);
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return this.delegate.getLogWriter();
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        this.delegate.setLogWriter(out);
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        this.delegate.setLoginTimeout(seconds);
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return this.delegate.getLoginTimeout();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return this.delegate.getParentLogger();
    }

    @Override
    public boolean shouldClose(Connection con) {
        return false;
    }

}