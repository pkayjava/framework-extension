package com.angkorteam.framework;

/**
 * Created by socheatkhauv on 6/13/17.
 */
public enum ProgressBarSize {

    SM("progress-sm"),

    XS("progress-xs"),

    XXS("progress-xxs");

    private String literal;

    ProgressBarSize(String literal) {
        this.literal = literal;
    }

    public String getLiteral() {
        return literal;
    }
}
