package com.angkorteam.framework.models;

import com.angkorteam.framework.Emoji;
import com.angkorteam.framework.EmojiColor;
import org.apache.wicket.Page;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.io.Serializable;

/**
 * Created by socheatkhauv on 6/13/17.
 */
public class UserInfo implements Serializable {

    private String image;

    private IModel<String> title;

    private IModel<String> description;

    private Class<? extends Page> page;

    private PageParameters parameters;

    private Emoji icon;

    private EmojiColor iconColor;

    public UserInfo setIcon(Emoji icon, EmojiColor color) {
        this.icon = icon;
        this.iconColor = color;
        return this;
    }

    public String getImage() {
        return image;
    }

    public UserInfo setImage(String image) {
        this.image = image;
        return this;
    }

    public String getTitle() {
        if (this.title == null) {
            return null;
        }
        return title.getObject();
    }

    public UserInfo setTitle(String title) {
        this.title = Model.of(title);
        return this;
    }

    public UserInfo setTitle(IModel<String> title) {
        this.title = title;
        return this;
    }

    public String getDescription() {
        if (description == null) {
            return null;
        }
        return description.getObject();
    }

    public UserInfo setDescription(String description) {
        this.description = Model.of(description);
        return this;
    }

    public UserInfo setDescription(IModel<String> description) {
        this.description = description;
        return this;
    }

    public Class<? extends Page> getPage() {
        return page;
    }

    public UserInfo setPage(Class<? extends Page> page) {
        this.page = page;
        return this;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public UserInfo setParameters(PageParameters parameters) {
        this.parameters = parameters;
        return this;
    }

    public Emoji getIcon() {
        return icon;
    }

    public EmojiColor getIconColor() {
        return iconColor;
    }
}
