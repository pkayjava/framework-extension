package com.angkorteam.framework.models;

import java.io.Serializable;

import org.apache.wicket.Page;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.angkorteam.framework.Emoji;
import com.angkorteam.framework.EmojiColor;
import com.angkorteam.framework.NavBarMenuItemType;
import com.angkorteam.framework.ProgressBarColor;
import com.angkorteam.framework.panels.UserItemPanel;

/**
 * Created by socheatkhauv on 6/12/17.
 */
public class NavBarMenuItem implements Serializable {

	private Emoji icon;

	private EmojiColor iconColor;

	private IModel<String> label;

	private IModel<String> description;

	private IModel<String> image;

	private IModel<String> small;

	private Emoji smallIcon;

	private EmojiColor smallIconColor;

	private IModel<Integer> progressBarValue;

	private ProgressBarColor progressBarColor;

	private Class<? extends Page> page;

	private PageParameters parameters;

	private Panel panel;

	private NavBarMenuItemType type;

	private NavBarMenuItem() {
	}

	public Emoji getIcon() {
		return icon;
	}

	public ProgressBarColor getProgressBarColor() {
		return progressBarColor;
	}

	public Class<? extends Page> getPage() {
		return page;
	}

	public PageParameters getParameters() {
		return parameters;
	}

	public EmojiColor getIconColor() {
		return iconColor;
	}

	public IModel<String> getLabel() {
		return label;
	}

	public Panel getPanel() {
		return panel;
	}

	public IModel<String> getDescription() {
		return description;
	}

	public NavBarMenuItem setDescription(String description) {
		this.description = Model.of(description);
		return this;
	}

	public IModel<Integer> getProgressBarValue() {
		return progressBarValue;
	}

	public IModel<String> getImage() {
		return image;
	}

	public IModel<String> getSmall() {
		return small;
	}

	public Emoji getSmallIcon() {
		return smallIcon;
	}

	public EmojiColor getSmallIconColor() {
		return smallIconColor;
	}

	public NavBarMenuItemType getType() {
		return type;
	}

	public static class Builder {

		private Emoji icon;

		private EmojiColor iconColor;

		private IModel<String> label;

		private IModel<String> description;

		private IModel<String> image;

		private IModel<String> small;

		private Emoji smallIcon;

		private EmojiColor smallIconColor;

		private IModel<Integer> progressBarValue;

		private ProgressBarColor progressBarColor;

		private Class<? extends Page> page;

		private PageParameters parameters;

		private Panel panel;

		private NavBarMenuItemType type;

		public Builder() {
		}

		public NavBarMenuItem build() {
			NavBarMenuItem item = new NavBarMenuItem();
			item.icon = this.icon;
			item.iconColor = this.iconColor;
			item.label = this.label;
			item.description = this.description;
			item.image = this.image;
			item.small = this.small;
			item.smallIcon = this.smallIcon;
			item.smallIconColor = this.smallIconColor;
			item.progressBarValue = this.progressBarValue;
			item.progressBarColor = this.progressBarColor;
			item.page = this.page;
			item.parameters = this.parameters;
			item.panel = this.panel;
			item.type = this.type;
			return item;
		}

		public Builder withImage(IModel<String> image, IModel<String> label, Class<? extends Page> page) {
			return withImage(image, label, page, new PageParameters(), Model.of(""));
		}

		public Builder withImage(IModel<String> image, IModel<String> label, Class<? extends Page> page,
				PageParameters parameters, IModel<String> description) {
			this.type = NavBarMenuItemType.ItemImage;
			this.label = label;
			this.page = page;
			this.image = image;
			this.parameters = parameters;
			this.description = description;
			return this;
		}

		public Builder withSmall(IModel<String> small, Emoji icon, EmojiColor color) {
			this.small = small;
			this.smallIcon = icon;
			this.smallIconColor = color;
			return this;
		}

		public Builder withIcon(Emoji icon, IModel<String> label, Class<? extends Page> page) {
			return withIcon(icon, label, page, new PageParameters(), EmojiColor.Red);
		}

		public Builder withIcon(Emoji icon, IModel<String> label, Class<? extends Page> page, PageParameters parameters,
				EmojiColor iconColor) {
			this.type = NavBarMenuItemType.ItemIcon;
			this.label = label;
			this.page = page;
			this.icon = icon;
			this.iconColor = iconColor;
			this.parameters = parameters;
			return this;
		}

		public Builder withPanel(UserItemPanel panel) {
			this.type = NavBarMenuItemType.ItemPanel;
			this.panel = panel;
			return this;
		}

		public Builder withProgressBar(IModel<Integer> progressBarValue, IModel<String> label,
				Class<? extends Page> page) {
			return withProgressBar(progressBarValue, label, page, new PageParameters(), ProgressBarColor.Red);
		}

		public Builder withProgressBar(IModel<Integer> progressBarValue, IModel<String> label,
				Class<? extends Page> page, PageParameters parameters, ProgressBarColor progressBarColor) {
			this.type = NavBarMenuItemType.ItemProgressBar;
			this.label = label;
			this.page = page;
			this.parameters = parameters;
			this.progressBarValue = progressBarValue;
			this.progressBarColor = progressBarColor;
			return this;
		}

	}

}
