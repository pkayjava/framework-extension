package com.angkorteam.framework.models;

import java.io.Serializable;
import java.util.Comparator;

import org.apache.wicket.model.IModel;

import com.angkorteam.framework.BackgroundColor;

public class Badge implements Serializable, Comparator<Badge>, Comparable<Badge> {

	private IModel<String> badgeText;

	private BackgroundColor badgeColor = BackgroundColor.Blue;

	private Integer order;

	private Badge() {
	}

	public IModel<String> getBadgeText() {
		return badgeText;
	}

	public BackgroundColor getBadgeColor() {
		return badgeColor;
	}

	@Override
	public int compareTo(Badge o) {
		return this.order.compareTo(o.order);
	}

	@Override
	public int compare(Badge o1, Badge o2) {
		return o1.order.compareTo(o2.order);
	}

	public static class Builder {

		private IModel<String> badgeText;

		private BackgroundColor badgeColor = BackgroundColor.Blue;

		private Integer order;

		public Badge build() {
			Badge item = new Badge();
			item.order = this.order;
			item.badgeText = this.badgeText;
			item.badgeColor = this.badgeColor;
			return item;
		}

		public Builder with(int order, IModel<String> badgeText, BackgroundColor badgeColor) {
			this.badgeText = badgeText;
			this.badgeColor = badgeColor;
			this.order = order;
			return this;
		}
	}
}