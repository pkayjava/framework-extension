package com.angkorteam.framework.models;

import java.io.Serializable;

import org.apache.wicket.model.IModel;

import com.angkorteam.framework.CalloutType;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class Callout implements Serializable {

    private CalloutType type;

    private IModel<String> title;

    private IModel<String> description;

    private Callout() {
    }

    public CalloutType getType() {
        return type;
    }

    public IModel<String> getTitle() {
        return title;
    }

    public IModel<String> getDescription() {
        return description;
    }

    public static class Builder {

        private CalloutType type;

        private IModel<String> title;

        private IModel<String> description;

        public Builder withType(CalloutType type) {
            this.type = type;
            return this;
        }

        public Builder withTitle(IModel<String> title) {
            this.title = title;
            return this;
        }

        public Builder withDescription(IModel<String> description) {
            this.description = description;
            return this;
        }

        public Callout build() {
            Callout item = new Callout();
            item.type = this.type;
            item.title = this.title;
            item.description = this.description;
            return item;
        }

    }

}
