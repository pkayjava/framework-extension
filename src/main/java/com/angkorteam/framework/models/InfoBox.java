package com.angkorteam.framework.models;

import java.io.Serializable;

import org.apache.wicket.Page;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.angkorteam.framework.BackgroundColor;
import com.angkorteam.framework.Emoji;
import com.angkorteam.framework.panels.InfoBoxExtraPanel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class InfoBox implements Serializable {

    private BackgroundColor backgroundColor;

    private Emoji icon;

    private IModel<String> title;

    private IModel<String> description;

    private Class<? extends Page> page;

    private PageParameters parameters;

    private InfoBoxExtraPanel extra;

    private InfoBox() {
    }

    public InfoBoxExtraPanel getExtra() {
        return extra;
    }

    public BackgroundColor getBackgroundColor() {
        return backgroundColor;
    }

    public Class<? extends Page> getPage() {
        return page;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public Emoji getIcon() {
        return icon;
    }

    public IModel<String> getTitle() {
        return title;
    }

    public IModel<String> getDescription() {
        return description;
    }

    public static class Builder {

        private BackgroundColor backgroundColor;

        private Emoji icon;

        private IModel<String> title;

        private IModel<String> description;

        private Class<? extends Page> page;

        private PageParameters parameters;

        private InfoBoxExtraPanel extra;

        public InfoBox build() {
            InfoBox item = new InfoBox();
            item.description = this.description;
            item.title = this.title;
            item.icon = this.icon;
            item.page = this.page;
            item.parameters = this.parameters;
            item.extra = this.extra;
            item.backgroundColor = this.backgroundColor;
            return item;
        }

        public Builder withExtra(InfoBoxExtraPanel extra) {
            this.extra = extra;
            return this;
        }

        public Builder withBackgroundColor(BackgroundColor backgroundColor) {
            this.backgroundColor = backgroundColor;
            return this;
        }

        public Builder withDescription(IModel<String> description) {
            this.description = description;
            return this;
        }

        public Builder withTitle(IModel<String> title) {
            this.title = title;
            return this;
        }

        public Builder withIcon(Emoji icon) {
            this.icon = icon;
            return this;
        }

        public Builder withPage(Class<? extends Page> page) {
            return withPage(page, new PageParameters());
        }

        public Builder withPage(Class<? extends Page> page, PageParameters parameters) {
            this.page = page;
            this.parameters = parameters;
            return this;
        }

    }

}
