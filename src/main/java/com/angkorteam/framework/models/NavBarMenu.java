package com.angkorteam.framework.models;

import java.io.Serializable;
import java.util.List;

import org.apache.wicket.Page;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.angkorteam.framework.BadgeType;
import com.angkorteam.framework.Emoji;
import com.angkorteam.framework.EmojiColor;
import com.angkorteam.framework.NavBarMenuType;
import com.angkorteam.framework.panels.UserFooterPanel;
import com.angkorteam.framework.panels.UserHeaderPanel;

/**
 * Created by socheatkhauv on 6/12/17.
 */
public class NavBarMenu implements Serializable {

    private Emoji icon;

    private EmojiColor iconColor;

    private IModel<String> badge;

    private BadgeType badgeType;

    private IModel<String> label;

    private IModel<String> image;

    private Class<? extends Page> page;

    private PageParameters parameters;

    private List<NavBarMenuItem> children;

    private NavBarMenuType type;

    private IModel<String> headerText;

    private Panel headerPanel;

    private IModel<String> footerText;

    private Panel footerPanel;

    private Class<? extends Page> footerPage;

    private PageParameters footerParameters;

    private NavBarMenu() {
    }

    public Emoji getIcon() {
        return this.icon;
    }

    public IModel<String> getBadge() {
        return this.badge;
    }

    public BadgeType getBadgeType() {
        return this.badgeType;
    }

    public EmojiColor getIconColor() {
        return this.iconColor;
    }

    public IModel<String> getLabel() {
        return this.label;
    }

    public Class<? extends Page> getPage() {
        return page;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public List<NavBarMenuItem> getChildren() {
        return children;
    }

    public NavBarMenuType getType() {
        return type;
    }

    public IModel<String> getImage() {
        return image;
    }

    public IModel<String> getHeaderText() {
        return this.headerText;
    }

    public Panel getHeaderPanel() {
        return headerPanel;
    }

    public Panel getFooterPanel() {
        return footerPanel;
    }

    public IModel<String> getFooterText() {
        return this.footerText;
    }

    public Class<? extends Page> getFooterPage() {
        return footerPage;
    }

    public PageParameters getFooterParameters() {
        return footerParameters;
    }

    public static class Builder {

        private Emoji icon;

        private EmojiColor iconColor;

        private IModel<String> badge;

        private BadgeType badgeType;

        private IModel<String> label;

        private IModel<String> image;

        private Class<? extends Page> page;

        private PageParameters parameters;

        private List<NavBarMenuItem> children;

        private NavBarMenuType type;

        private IModel<String> headerText;

        private Panel headerPanel;

        private IModel<String> footerText;

        private Panel footerPanel;

        private Class<? extends Page> footerPage;

        private PageParameters footerParameters;

        public Builder() {
        }

        public NavBarMenu build() {
            NavBarMenu item = new NavBarMenu();
            item.icon = this.icon;
            item.iconColor = this.iconColor;
            item.badge = this.badge;
            item.badgeType = this.badgeType;
            item.label = this.label;
            item.image = this.image;
            item.page = this.page;
            item.parameters = this.parameters;
            item.children = this.children;
            item.type = this.type;
            item.headerText = this.headerText;
            item.headerPanel = this.headerPanel;
            item.footerText = this.footerText;
            item.footerPanel = this.footerPanel;
            item.footerPage = this.footerPage;
            item.footerParameters = this.footerParameters;
            return item;
        }

        public Builder withHeader(IModel<String> label) {
            this.headerText = label;
            this.headerPanel = null;
            return this;
        }

        public Builder withHeader(UserHeaderPanel header) {
            this.headerText = null;
            this.headerPanel = header;
            return this;
        }

        public Builder withFooter(IModel<String> label) {
            this.footerText = label;
            this.footerPage = null;
            this.footerParameters = null;
            this.footerPanel = null;
            return this;
        }

        public Builder withFooter(IModel<String> label, Class<? extends Page> page) {
            return withFooter(label, page, new PageParameters());
        }

        public Builder withFooter(IModel<String> label, Class<? extends Page> page, PageParameters parameters) {
            this.footerText = label;
            this.footerPage = page;
            this.footerParameters = parameters;
            this.footerPanel = null;
            return this;
        }

        public Builder withFooter(UserFooterPanel footer) {
            this.footerText = null;
            this.footerPage = null;
            this.footerParameters = null;
            this.footerPanel = footer;
            return this;
        }

        public Builder withIcon(Emoji icon, IModel<String> label, Class<? extends Page> page) {
            return withIcon(icon, label, page, Model.of(""));
        }

        public Builder withIcon(Emoji icon, IModel<String> label, Class<? extends Page> page, IModel<String> badge) {
            return withIcon(icon, label, page, badge, new PageParameters(), EmojiColor.Gray, BadgeType.Danger);
        }

        public Builder withIcon(Emoji icon, IModel<String> label, Class<? extends Page> page, IModel<String> badge, PageParameters parameters, EmojiColor color, BadgeType badgeType) {
            this.type = NavBarMenuType.ItemIcon;
            this.page = page;
            this.parameters = parameters;
            this.icon = icon;
            this.iconColor = color;
            this.label = label;
            this.badge = badge;
            this.badgeType = badgeType;
            return this;
        }

        public Builder withIcon(Emoji icon, IModel<String> label, List<NavBarMenuItem> items) {
            return withIcon(icon, label, items, Model.of(""));
        }

        public Builder withIcon(Emoji icon, IModel<String> label, List<NavBarMenuItem> items, IModel<String> badge) {
            return withIcon(icon, label, items, badge, EmojiColor.Gray, BadgeType.Danger);
        }

        public Builder withIcon(Emoji icon, IModel<String> label, List<NavBarMenuItem> items, IModel<String> badge, EmojiColor color, BadgeType badgeType) {
            this.type = NavBarMenuType.ItemIcon;
            this.icon = icon;
            this.label = label;
            this.badge = badge;
            this.iconColor = color;
            this.children = items;
            this.badgeType = badgeType;
            return this;
        }

        public Builder withImage(IModel<String> image, IModel<String> label, Class<? extends Page> page) {
            return withImage(image, label, page, Model.of(""));
        }

        public Builder withImage(IModel<String> image, IModel<String> label, Class<? extends Page> page, IModel<String> badge) {
            return withImage(image, label, page, badge, new PageParameters(), Model.of(BadgeType.Danger));
        }

        public Builder withImage(IModel<String> image, IModel<String> label, Class<? extends Page> page, IModel<String> badge, PageParameters parameters, IModel<BadgeType> badgeType) {
            this.type = NavBarMenuType.ItemImage;
            this.page = page;
            this.parameters = parameters;
            this.image = image;
            this.label = label;
            this.badge = badge;
            return this;
        }

        public Builder withImage(IModel<String> image, IModel<String> label, List<NavBarMenuItem> items) {
            return withImage(image, label, items, Model.of(""));
        }

        public Builder withImage(IModel<String> image, IModel<String> label, List<NavBarMenuItem> items, IModel<String> badge) {
            return withImage(image, label, items, badge, BadgeType.Danger);
        }

        public Builder withImage(IModel<String> image, IModel<String> label, List<NavBarMenuItem> items, IModel<String> badge, BadgeType badgeType) {
            this.type = NavBarMenuType.ItemImage;
            this.image = image;
            this.label = label;
            this.badge = badge;
            this.children = items;
            this.badgeType = badgeType;
            return this;
        }

    }

}
