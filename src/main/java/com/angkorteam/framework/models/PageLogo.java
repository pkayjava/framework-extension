package com.angkorteam.framework.models;

import java.io.Serializable;

import org.apache.wicket.Page;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class PageLogo implements Serializable {

	private Class<? extends Page> page;

	private PageParameters parameters;

	private IModel<String> small;

	private IModel<String> large;

	private PageLogo() {
	}

	public Class<? extends Page> getPage() {
		return page;
	}

	public PageParameters getParameters() {
		return parameters;
	}

	public IModel<String> getSmall() {
		return small;
	}

	public IModel<String> getLarge() {
		return large;
	}

	public static class Builder {

		private Class<? extends Page> page;

		private PageParameters parameters;

		private IModel<String> small;

		private IModel<String> large;

		public PageLogo build() {
			PageLogo item = new PageLogo();
			item.page = this.page;
			item.parameters = this.parameters;
			item.small = this.small;
			item.large = this.large;
			return item;
		}

		public Builder with(IModel<String> small, IModel<String> large) {
			return with(small, large, null);
		}

		public Builder with(IModel<String> small, IModel<String> large, Class<? extends Page> page) {
			return with(small, large, page, null);
		}

		public Builder with(IModel<String> small, IModel<String> large, Class<? extends Page> page, PageParameters parameters) {
			this.small = small;
			this.large = large;
			this.page = page;
			this.parameters = parameters;
			return this;
		}

	}

}
