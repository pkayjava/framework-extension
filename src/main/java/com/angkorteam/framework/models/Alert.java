package com.angkorteam.framework.models;

import java.io.Serializable;

import org.apache.wicket.model.IModel;

import com.angkorteam.framework.AlertType;
import com.angkorteam.framework.Emoji;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class Alert implements Serializable {

    private AlertType type;

    private Emoji icon;

    private IModel<String> title;

    private IModel<String> description;

    private Alert() {
    }

    public AlertType getType() {
        return type;
    }

    public Emoji getIcon() {
        return icon;
    }

    public IModel<String> getTitle() {
        return title;
    }

    public IModel<String> getDescription() {
        return description;
    }

    public static class Builder {

        private AlertType type;

        private Emoji icon;

        private IModel<String> title;

        private IModel<String> description;

        public Builder() {
        }

        public Builder withType(AlertType type) {
            this.type = type;
            return this;
        }

        public Builder withEmoji(Emoji icon) {
            this.icon = icon;
            return this;
        }

        public Builder withTitle(IModel<String> title) {
            this.title = title;
            return this;
        }

        public Builder withDescription(IModel<String> description) {
            this.description = description;
            return this;
        }

        public Alert build() {
            Alert alert = new Alert();
            alert.type = type;
            alert.icon = icon;
            alert.title = title;
            alert.description = description;
            return alert;
        }

    }

}
