package com.angkorteam.framework.models;

import java.io.Serializable;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.angkorteam.framework.Emoji;

/**
 * Created by socheatkhauv on 6/11/17.
 */
public class PageBreadcrumb implements Serializable {

    private IModel<String> label;

    private Emoji icon;

    private Class<? extends WebPage> page;

    private PageParameters parameters;

    private boolean active;

    private PageBreadcrumb() {
    }

    public IModel<String> getLabel() {
        return label;
    }

    public boolean isActive() {
        return active;
    }

    public Emoji getIcon() {
        return icon;
    }

    public Class<? extends WebPage> getPage() {
        return page;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public static class Builder {

        private IModel<String> label;

        private Emoji icon;

        private Class<? extends WebPage> page;

        private PageParameters parameters;

        private boolean active;

        public PageBreadcrumb build() {
            PageBreadcrumb item = new PageBreadcrumb();
            item.label = this.label;
            item.icon = this.icon;
            item.page = this.page;
            item.parameters = this.parameters;
            item.active = this.active;
            return item;
        }

        public Builder withLabel(IModel<String> label) {
            this.label = label;
            return this;
        }

        public Builder withActive(boolean active) {
            this.active = active;
            return this;
        }

        public Builder withIcon(Emoji icon) {
            this.icon = icon;
            return this;
        }

        public Builder withPage(Class<? extends WebPage> page) {
            return withPage(page, new PageParameters());
        }

        public Builder withPage(Class<? extends WebPage> page, PageParameters parameters) {
            this.page = page;
            this.parameters = parameters;
            return this;
        }

    }

}
