package com.angkorteam.framework.models;

import java.io.Serializable;

import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class PageFooter implements Serializable {

    private IModel<String> company;

    private PageFooter() {
    }

    public IModel<String> getCompany() {
        return company;
    }

    public static class Builder {

        private IModel<String> company;

        public PageFooter build() {
            PageFooter item = new PageFooter();
            item.company = this.company;
            return item;
        }

        public Builder withCompany(IModel<String> company) {
            this.company = company;
            return this;
        }

    }

}
