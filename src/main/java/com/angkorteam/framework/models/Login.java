package com.angkorteam.framework.models;

import java.io.Serializable;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class Login implements Serializable {

    private String login;

    private String password;

    private boolean remember;

    public Login(String login, String password, boolean remember) {
        this.login = login;
        this.password = password;
        this.remember = remember;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public boolean isRemember() {
        return remember;
    }
}
