package com.angkorteam.framework.models;

import java.io.Serializable;

import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/11/17.
 */
public class PageHeader implements Serializable {

    private IModel<String> title;

    private IModel<String> description;

    private PageHeader() {
    }

    public IModel<String> getTitle() {
        return title;
    }

    public IModel<String> getDescription() {
        return description;
    }

    public static class Builder {

        private IModel<String> title;

        private IModel<String> description;

        public PageHeader build() {
            PageHeader item = new PageHeader();
            item.title = this.title;
            item.description = this.description;
            return item;
        }

        public Builder withTitle(IModel<String> title) {
            this.title = title;
            return this;
        }

        public Builder withDescription(IModel<String> description) {
            this.description = description;
            return this;
        }

    }

}
